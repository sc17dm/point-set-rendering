#pragma once

#include "vertex.h"

class Sphere{
public:
    Vertex* position;
    float radius;
    Vertex colour;
    Vertex* normal;
    float* params;
    Sphere** children;
    int level;
    
    Sphere(Vertex* pos, float rad, Vertex col, Vertex* norm, float* prms);

    Sphere(Sphere* ch[4]);

    static Sphere* buildTree(Vertex** verts, int begin, int end, float h);

    static void partitionAlongLongestAxis(Vertex** verts, int begin, int end, int indices[4]);
};