#pragma once
#include "vertex.h"
#include <vector>
#include <iostream>
#include <cmath>

class Octree{
public:
    Octree();
    Octree(Vertex position, float size);
    Vertex* centroid; // irrelevant for leaf nodes
    Vertex* point; // empty for branch nodes
    Octree** children; // up to 8 children, counting from bottom front left corner, counter clockwise
    int childrenCount;
    int pointCount;
    Vertex position;
    float size;
    int hasPoint;
    bool isRetVal;
    
    
    void addVertex(Vertex* vert);
    void createChildren();
    void computeCentroid();
    void computeCoords(Vertex* corner, int* index, Vertex vert, float size);
    
};



