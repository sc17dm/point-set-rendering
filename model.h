#pragma once
#include "vertex.h"
#include "utils.h"
#include "grid.h"
#include "sphere.h"
#include <GL/glu.h>
#include <GL/glut.h>
#include <vector>
#include <limits.h>

class Model{
    public:
        Vertex **verts;
        Sphere* root;
        Grid partitionGrid;
        Vertex viewDir;
        std::vector <Vertex*> vertices;
        float epsilon;
        int n;
        int faces;
        Vertex** triangles;
        int level;
        int count;
        float angle;
        float scale;
        float radius;
        float h;
        float negl;
        int downSample, upSample;
        bool shouldRenderPoints, shouldRenderProjPoints, shouldRenderRepresentationPoints, shouldRenderOctrees, shouldRenderNormals, shouldRenderCompNormals, shouldRenderSurface, shouldRenderSpheres;
        Vertex eye;
        GLfloat mvMat[16]; 

        Model();
        Model(char* inputFile);
        void read(char* inputFile);
        void printDebug();
};