#pragma once

#include <QGLWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QSlider>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include "PSRWidget.h"
#include "model.h"

class PSRWindow: public QWidget
{ 
	public:
	PSRWindow(Model *model, QWidget *parent);
	~PSRWindow();

	Model *model;
	PSRWidget *psrWidget;

	QHBoxLayout *windowLayout;
	QVBoxLayout *widgetLayout;
	QVBoxLayout *optionLayout;
	QVBoxLayout *renderOptionLayout;
	QVBoxLayout *projectionOptionLayout;
	
	QGroupBox *optionGroup;
	QGroupBox *widgetGroup;
	QGroupBox *projectionOptionGroup;
	QGroupBox *renderOptionGroup;

	QSlider *scaleSlider;
	QSlider *levelSlider;
	QSlider *downSlider;
	QSlider *upSlider;
	QSlider *radiusSlider;
	QSlider *hSlider;

	QLabel *scaleLabel;
	QLabel *levelLabel;
	QLabel *downLabel;
	QLabel *upLabel;
	QLabel *radiusLabel;
	QLabel *hLabel;

	QPushButton *projectSurfButton;
	QPushButton *exportPPMButton;

	QCheckBox *renderPointButton;
	QCheckBox *renderProjPointButton;
	QCheckBox *renderOctreeButton;
	QCheckBox *renderRepresentationPointButton;
	QCheckBox *renderNormalButton;
	QCheckBox *renderCompNormalButton;
	QCheckBox *renderSpheresButton;
	QCheckBox *renderSurfaceButton;

	void ResetInterface();
}; 
