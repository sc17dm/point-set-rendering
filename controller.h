#pragma once

#include "model.h"
#include "PSRWindow.h"

class Controller : public QObject{
Q_OBJECT
public:
    Model *model;
    PSRWindow *view;

    Controller(Model *newModel, PSRWindow *newView);
    ~Controller(){};

public slots:
    void scaleChanged(int newValue);
    void levelChanged(int newValue);
    void downChanged(int newValue);
    void upChanged(int newValue);
    void pointChanged(int newValue);
    void projPointChanged(int newValue);
    void representationChanged(int newValue);
    void octreeChanged(int newValue);
    void normalChanged(int newValue);
    void compNormalChanged(int newValue);
    void surfaceChanged(int newValue);
    void spheresChanged(int newValue);
    void projectSurfaceChanged();
    void exportPPMChanged();
    void hChanged(int newValue);
    void radiusChanged(int newValue);
};
