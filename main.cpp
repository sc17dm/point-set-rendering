#include "main.h"


int main(int argc, char *argv[]){

    if(argc != 2){
        std::cout << "Incorrect number of parameters.\n\nUsage: ./pointSetRendering fileName\n";

        return 0;
    }

    Model* model = new Model(argv[1]);
    

    QApplication app(argc, argv);
    glutInit(&argc, argv);
    PSRWindow* window = new PSRWindow(model, nullptr);
    
    Controller* controller = new Controller(model, window);

    window->resize(800, 600);
    window->setWindowTitle("Point Set Rendering");
    window->show();

    return app.exec();
}