#include "model.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <string.h>
#include <chrono>

#define NEGLECTDIST 0.002f

Model::Model(){
    verts = nullptr;
    n = 0;
    angle = 0;
    eye.x = eye.y = eye.z = 0;
    scale = 50;
    h = 1;
    shouldRenderPoints = shouldRenderRepresentationPoints = shouldRenderOctrees = shouldRenderNormals = shouldRenderCompNormals = shouldRenderSurface = shouldRenderSpheres = false;
    root = nullptr;
    downSample = upSample = 0;
}

Model::Model(char* inputFile){
    verts = nullptr;
    n = 0;
    angle = 0;
    level = 0;
    eye.x = 0;
    eye.y = -8;
    eye.z = 0;
    scale = 50;
    h = 0.0015;
    radius = 0.0001;
    epsilon = .000001;
    count = 0;
    upSample = downSample = 0;
    shouldRenderPoints = shouldRenderProjPoints = shouldRenderRepresentationPoints = shouldRenderOctrees = shouldRenderNormals = shouldRenderCompNormals = shouldRenderSurface = shouldRenderSpheres = false;
    downSample = upSample = 0;
    viewDir = Vertex(-1,-10,-10);
    viewDir = viewDir.normalize();

    read(inputFile);
    root = Sphere::buildTree(verts, 0, n, h);
    downSampler(verts, n, 71, &partitionGrid);
    upSampler(verts, vertices, n, 275, &partitionGrid);
}


void printOctree(Octree* oct){
    if(oct->isRetVal)
        return;
    if(!oct->hasPoint)
        return;
    if(oct->point != nullptr){
        std::cout << *oct->point << std::endl;
        std::cout << oct->size << std::endl;
        return;
    }
    for(int i = 0; i < 8; ++i){
        std::cout << "Chidren: " << i << std::endl;
        printOctree(oct->children[i]);
    }
}

void Model::read(char* inputFile){
    std::ifstream input;
    input.open(inputFile);
    std::string temp = "default";
    std::string extension;
    float xMin, yMin, zMin, xMax, yMax, zMax;
    int props = 0;
    float extra;

    xMin = yMin = zMin = INT_MAX; 
    xMax = yMax = zMax = INT_MIN;

    extension = (strrchr(inputFile, '.') == nullptr ? "default" : strrchr(inputFile, '.') );
    if(extension == ".ply"){
        while (temp != "vertex"){
            input >> temp;
        }
    
        input >> n;

    
        while (temp != "end_header"){
            if(temp == "property"){
                ++props;
            }
            if(temp == "face"){
                input >> faces;
            }
            input >> temp;
        }

        std::cout << "N: " << n << std::endl;
        props-=4;
    
        verts = new Vertex*[n];
        float x,y,z;
    
        for(int i = 0; i < n; ++i){
            input >> x >> y >> z;
            for(int j = 0; j < props; ++j){
                input >> extra;
            }
            verts[i] = new Vertex(x,y,z);
            verts[i]->normal = new Vertex(0,1,0);
            verts[i]->projection = new Vertex(x,y,z);
            verts[i]->params = new float[10];
            if(x < xMin){
                xMin = x;
            }
            if(y < yMin){
                yMin = y;
            }
            if(z < zMin){
                zMin = z;
            }

            if(x > xMax){
                xMax = x;
            }
            if(y > yMax){
                yMax = y;
            }
            if(z > zMax){
                zMax = z;
            }
        }

        triangles = new Vertex*[faces];
        int tempo;
        int a,b,c;
        for(int i = 0; i < faces; ++i){
            input >> tempo >> a >> b >> c;
            triangles[i] = new Vertex(a,b,c);
        }

        partitionGrid = Grid(xMax-xMin, yMax-yMin, zMax - zMin, NEGLECTDIST, xMin, yMin, zMin);

        std::cout <<"Grid size: " << partitionGrid.xSize << ' ' << partitionGrid.ySize << ' ' << partitionGrid.zSize << std::endl;
        // std::cout <<"Minimum: " <<  partitionGrid.xMin << ' ' << partitionGrid.yMin << ' ' << partitionGrid.zMin << std::endl;

        for(int i = 0; i < n; ++i){
            int x,y,z;


            x = (verts[i]->x - partitionGrid.xMin)/partitionGrid.neglDist;
            y = (verts[i]->y - partitionGrid.yMin)/partitionGrid.neglDist;
            z = (verts[i]->z - partitionGrid.zMin)/partitionGrid.neglDist;


            if(x >= partitionGrid.xSize)
                --x;
            if(y >= partitionGrid.ySize)
                --y;
            if(z >= partitionGrid.zSize)
                --z;

            partitionGrid(x,y,z)->addVertex(verts[i]);

        }
    }
    else if (extension == ".test"){
        input >> n;
        verts = new Vertex*[n];
        std::cout << "N: " << n << std::endl;

        float x,y,z;
        
        for(int i = 0; i < n; ++i){
            input >> x >> y >> z;
            verts[i] = new Vertex(x,y,z);
            verts[i]->normal = new Vertex(0,10,0);
            verts[i]->projection= new Vertex(x,y,z);
            verts[i]->params = new float[10];

            if(x < xMin){
                xMin = x;
            }
            if(y < yMin){
                yMin = y;
            }
            if(z < zMin){
                zMin = z;
            }

            if(x > xMax){
                xMax = x;
            }
            if(y > yMax){
                yMax = y;
            }
            if(z > zMax){
                zMax = z;
            }
        }

        partitionGrid = Grid(xMax-xMin, yMax-yMin, zMax - zMin, NEGLECTDIST, xMin, yMin, zMin);

        std::cout <<"Grid size: " << partitionGrid.xSize << ' ' << partitionGrid.ySize << ' ' << partitionGrid.zSize << std::endl;
        // std::cout <<"Minimum: " <<  partitionGrid.xMin << ' ' << partitionGrid.yMin << ' ' << partitionGrid.zMin << std::endl;
        // std::cout << "h: " << h << " negl: " << negl << std::endl;

        for(int i = 0; i < n; ++i){
            int x,y,z;

            x = (verts[i]->x - partitionGrid.xMin)/partitionGrid.neglDist;
            y = (verts[i]->y - partitionGrid.yMin)/partitionGrid.neglDist;
            z = (verts[i]->z - partitionGrid.zMin)/partitionGrid.neglDist;


            if(x >= partitionGrid.xSize)
                --x;
            if(y >= partitionGrid.ySize)
                --y;
            if(z >= partitionGrid.zSize)
                --z;
            partitionGrid(x,y,z)->addVertex(verts[i]);
        }
    }
}

void Model::printDebug(){
    std::cout << "Vertices in current file: " << n << std::endl;

    for(int i = 0; i < n; ++i){
        std::cout << verts[i]->x << ' ' << verts[i]->y << ' ' << verts[i]->z << std::endl;
    }
}

