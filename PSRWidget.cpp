#include "PSRWidget.h"
#include <iostream>

#define PATCHSIZE model->h
#define REPRDIST 0.015

material whiteShinyMat = {
    { 0.1, 0.1, 0.1, 1.0}, //ambient
    { 0.5, 0.5, 0.5, 1.0}, //diffuse
    { 0.9, 0.9, 0.9, 1.0}, //specular
    10.0  
};

material redMat = {
    { 0.2, 0.0, 0.0, 1.0}, //ambient
    { 1.0, 0.0, 0.0, 1.0}, //diffuse
    { 0.0, 0.0, 0.0, 1.0}, //specular
    0.0  
};

material blueMat = {
    { 0.0, 0.0, 0.2, 1.0}, //ambient
    { 0.0, 0.0, 1.0, 1.0}, //diffuse
    { 0.0, 0.0, 0.0, 1.0}, //specular
    0.0  
};

material greenMat = {
    { 0.0, 0.2, 0.0, 1.0}, //ambient
    { 0.0, 1.0, 0.0, 1.0}, //diffuse
    { 0.0, 0.0, 0.0, 1.0}, //specular
    0.0  
};

material purpleMat = {
    { 0.2, 0.0, 0.2, 1.0}, //ambient
    { 1.0, 0.0, 1.0, 1.0}, //diffuse
    { 0.0, 0.0, 0.0, 1.0}, //specular
    0.0  
};

material orangeMat = {
    { 0.2, 0.2, 0.0, 1.0}, //ambient
    { 1.0, 1.0, 0.0, 1.0}, //diffuse
    { 0.0, 0.0, 0.0, 1.0}, //specular
    0.0  
};

PSRWidget::PSRWidget(Model* model, QWidget* parent):QGLWidget(parent){
    this->model = model;
    setFocusPolicy(Qt::StrongFocus);
    counter = 0;
}

void PSRWidget::initializeGL(){
	glClearColor(0.3, 0.3, 0.3, 0.0); 
}

void PSRWidget::resizeGL(int w, int h){
	glViewport(0, 0, w, h);
    this->w = w;
    this->h = h;

    GLfloat lightPos[] = {1.,5.,10.,0.};

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_BLEND);
  
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (float)w/h, 0.01, 100);

	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);
    gluPerspective(60, (float)w/h, 0.01, 100);

}

void PSRWidget::paintGL(){ 
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_MODELVIEW);
    glTranslatef(model->eye.x, model->eye.y, model->eye.z);
    glRotatef(model->angle, 0., 1., 0.);
    glEnable(GL_NORMALIZE);
    this->loadVertices();
    // this->renderOriginalSurf(); // uncomment this and comment previous
                                   // line for triangulated surface on ply
    glDisable(GL_NORMALIZE);
    glLoadIdentity();
    gluLookAt(1.0, 5.0, 10.0, 0.0,0.0,0.0, 0.0,1.0,0.0);	
	glFlush();
}

void PSRWidget::drawOctree(Octree* oct){
    if(oct == nullptr)
        return;
    Vertex corner = oct->position;
    float size = oct->size;
    
    glVertex3f(corner.x * model->scale, corner.y * model->scale, corner.z * model->scale);
    glVertex3f(corner.x* model->scale, corner.y* model->scale, (corner.z+size)* model->scale);

    glVertex3f(corner.x* model->scale, corner.y* model->scale, corner.z* model->scale);
    glVertex3f(corner.x* model->scale, (corner.y+size)* model->scale, corner.z* model->scale);
    
    glVertex3f(corner.x* model->scale, corner.y* model->scale, corner.z* model->scale);
    glVertex3f((corner.x+ size)* model->scale, corner.y* model->scale, corner.z* model->scale);
    
    glVertex3f((corner.x+size)* model->scale, (corner.y+size)* model->scale, (corner.z+size)* model->scale);
    glVertex3f((corner.x+size)* model->scale, (corner.y+size)* model->scale, corner.z* model->scale);
    
    glVertex3f((corner.x+size)* model->scale, (corner.y+size)* model->scale, (corner.z+size)* model->scale);
    glVertex3f((corner.x+size)* model->scale, corner.y* model->scale, (corner.z+size)* model->scale);
    
    glVertex3f((corner.x+size)* model->scale, (corner.y+size)* model->scale, (corner.z+size)* model->scale);
    glVertex3f(corner.x* model->scale, (corner.y+size) * model->scale, (corner.z+size)* model->scale);
    
    glVertex3f(corner.x* model->scale, (corner.y+size)* model->scale, corner.z* model->scale);
    glVertex3f((corner.x+size)* model->scale, (corner.y+size)* model->scale, corner.z* model->scale);
    
    glVertex3f(corner.x* model->scale, (corner.y+size)* model->scale, corner.z* model->scale);
    glVertex3f(corner.x* model->scale, (corner.y+size)* model->scale, (corner.z+size)* model->scale);
    
    glVertex3f(corner.x* model->scale, corner.y* model->scale, (corner.z+size)* model->scale);
    glVertex3f(corner.x* model->scale, (corner.y+size)* model->scale, (corner.z+size)* model->scale);

    glVertex3f(corner.x* model->scale, corner.y* model->scale, (corner.z+size)* model->scale);
    glVertex3f((corner.x+size)* model->scale, corner.y* model->scale, (corner.z+size)* model->scale);   

    glVertex3f((corner.x+size)* model->scale, corner.y* model->scale, corner.z* model->scale);
    glVertex3f((corner.x+size)* model->scale, (corner.y+size)* model->scale, corner.z* model->scale);

    glVertex3f((corner.x+size)* model->scale, corner.y* model->scale, corner.z* model->scale);
    glVertex3f((corner.x+size)* model->scale, corner.y* model->scale, (corner.z+size)* model->scale);  

    for(int i = 0; i < 8; ++i){
        drawOctree(oct->children[i]);
    }
}

void PSRWidget::getNormal(Vertex* normal, Vertex* index){
    Vertex a = *model->verts[(int)(index->y)] - *model->verts[(int)(index->x)], b = *model->verts[(int)(index->z)] - *model->verts[(int)(index->y)];

    normal->x = a.y*b.z - a.z*b.y;
    normal->y = a.z*b.x - a.x*b.z;
    normal->z = a.x*b.y - a.y*b.x;

    *normal = normal->normalize();
}

void PSRWidget::renderOriginalSurf(){
    glMaterialfv(GL_FRONT, GL_AMBIENT, whiteShinyMat.ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteShinyMat.diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, whiteShinyMat.specular);
    glMaterialf(GL_FRONT, GL_SHININESS, whiteShinyMat.shininess);
    glBegin(GL_TRIANGLES);
    Vertex normal;
    for(int i = 0; i < model->faces; ++i){
        getNormal(&normal, model->triangles[i]);
        glNormal3f(normal.x, normal.y, normal.z);
        
        int index = (int)model->triangles[i]->x;
        glVertex3f(model->verts[index]->x * model->scale, model->verts[index]->y * model->scale, model->verts[index]->z * model->scale);

        index = (int)model->triangles[i]->y;
        glVertex3f(model->verts[index]->x * model->scale, model->verts[index]->y * model->scale, model->verts[index]->z * model->scale);


        index = (int)model->triangles[i]->z;
        glVertex3f(model->verts[index]->x * model->scale, model->verts[index]->y * model->scale, model->verts[index]->z * model->scale);

    }
    glEnd();
}

void PSRWidget::loadVertices(){
    if(model->shouldRenderPoints){   
        glMaterialfv(GL_FRONT, GL_AMBIENT, blueMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, blueMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, blueMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, blueMat.shininess);
        
        glBegin(GL_POINTS);

        for(int i = 0; i < model->n; ++i){
            if(31-model->verts[i]->shouldDraw >= model->downSample){
                glNormal3f(model->verts[i]->normal->x, model->verts[i]->normal->y, model->verts[i]->normal->z);
                glVertex3f(model->verts[i]->x * model->scale, model->verts[i]->y * model->scale, model->verts[i]->z * model->scale);
            }
        }

        glMaterialfv(GL_FRONT, GL_AMBIENT, blueMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, blueMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, blueMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, blueMat.shininess);

        for(size_t i = 0; i < model->vertices.size(); ++i){
            if(model->vertices[i]->upLevel <= model->upSample){
                glNormal3f(model->vertices[i]->normal->x, model->vertices[i]->normal->y, model->vertices[i]->normal->z);
                glVertex3f(model->vertices[i]->x * model->scale, model->vertices[i]->y * model->scale, model->vertices[i]->z * model->scale);
            }
        }

        glEnd();
    }

    if(model->shouldRenderRepresentationPoints){   
        glMaterialfv(GL_FRONT, GL_AMBIENT, purpleMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, purpleMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, purpleMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, purpleMat.shininess);
        
        glBegin(GL_POINTS);
        for(int i = 0; i < model->n; ++i){
            if(model->verts[i]->dist(*model->verts[i]->projection)< REPRDIST){
                glNormal3f(model->verts[i]->normal->x, model->verts[i]->normal->y, model->verts[i]->normal->z);
                glVertex3f(model->verts[i]->x * model->scale, model->verts[i]->y * model->scale, model->verts[i]->z * model->scale);
            }
            
        }
        glEnd();
    }

    if(model->shouldRenderProjPoints){   
        glMaterialfv(GL_FRONT, GL_AMBIENT, redMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, redMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, redMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, redMat.shininess);
        
        glBegin(GL_POINTS);
        for(int i = 0; i < model->n; ++i){
                glNormal3f(model->verts[i]->normal->x, model->verts[i]->normal->y, model->verts[i]->normal->z);
                glVertex3f(model->verts[i]->projection->x * model->scale, model->verts[i]->projection->y * model->scale, model->verts[i]->projection->z * model->scale);
            
            
        }
        glEnd();
    }

    if(model->shouldRenderOctrees){   
        glMaterialfv(GL_FRONT, GL_AMBIENT, orangeMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, orangeMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, orangeMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, orangeMat.shininess);
        
        glBegin(GL_LINES);
        glNormal3f(0,1,0);
        for(int i = 0; i < model->partitionGrid.xSize; ++i){
            for(int j = 0; j < model->partitionGrid.ySize; ++j){
                for(int k = 0; k < model->partitionGrid.zSize; ++k){
                    drawOctree(model->partitionGrid(i,j,k));
                }
            }            
        }
        glEnd();
    }

    if(model->shouldRenderCompNormals){
        glMaterialfv(GL_FRONT, GL_AMBIENT, redMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, redMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, redMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, redMat.shininess);
        
        glBegin(GL_LINES);
        for(int i = 0; i < model->n; ++i){
            if(model->shouldRenderRepresentationPoints){
                if(model->verts[i]->dist(*model->verts[i]->projection)< REPRDIST){
                glNormal3f(model->verts[i]->normal->x, model->verts[i]->normal->y, model->verts[i]->normal->z);
                glVertex3f(model->verts[i]->x * model->scale, model->verts[i]->y * model->scale, model->verts[i]->z * model->scale);
                glVertex3f((model->verts[i]->normal->x/200.0 + model->verts[i]->x)* model->scale, (model->verts[i]->normal->y/200.0 + model->verts[i]->y) * model->scale, (model->verts[i]->normal->z/200.0 + model->verts[i]->z) * model->scale);
                }
            }
            else{   
                glNormal3f(model->verts[i]->normal->x, model->verts[i]->normal->y, model->verts[i]->normal->z);
                glVertex3f(model->verts[i]->x * model->scale, model->verts[i]->y * model->scale, model->verts[i]->z * model->scale);
                glVertex3f((model->verts[i]->normal->x/200.0 + model->verts[i]->x)* model->scale, (model->verts[i]->normal->y/200.0 + model->verts[i]->y) * model->scale, (model->verts[i]->normal->z/200.0 + model->verts[i]->z) * model->scale);
            }            
        }
        glEnd();
    }

    if(model->shouldRenderSpheres){
        glMaterialfv(GL_FRONT, GL_AMBIENT, greenMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, greenMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, greenMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, greenMat.shininess);
        int count = 0;
        glBegin(GL_LINES);
        drawSpheres(model->root, &count);
        glEnd();
    }

    if(model->shouldRenderSurface){
    
        glMaterialfv(GL_FRONT, GL_AMBIENT, whiteShinyMat.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteShinyMat.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, whiteShinyMat.specular);
        glMaterialf(GL_FRONT, GL_SHININESS, whiteShinyMat.shininess);
           
        glBegin(GL_POINTS);
        drawSurface(model->root);
        glEnd();
    }
}

void PSRWidget::drawSurface(Sphere* sp){
        if(sp){
            float sine = sin(model->angle * PI / 180.0);
            float cosi = cos(model->angle * PI / 180.0);

            Vertex pointSize, prevPoint, radiusSize, normDir = Vertex(sp->normal->x * cosi + sp->normal->z * sine, sp->normal->y, -sp->normal->x * sine + sp->normal->z * cosi);
            
            normDir = normDir.normalize();
            
            // check for normal direction
            // if(normDir * model->viewDir <= -0.9)
            //     {
            //         return;
            //     }

           

            if(sp->children)
            {
                // std::cout << "has children: " << sp << " at position: " << *sp->position << " normal: " << *sp->normal <<std::endl;
                for(int i = 0; i < 4; ++i){
                    // std::cout << "child " << i << std::endl;
                    drawSurface(sp->children[i]);

                }
                // std::cout << "has finished children:" << sp << std::endl;
            }
            else{
                // if(sp->position->dist(*sp->position->projection) > REPRDIST)
                //     return;

                std::vector<Vertex*> nbr;
                float radius = 0.0;

                getClosestCells(&model->partitionGrid, sp->position, nbr, false);
                
                for(size_t j = 0; j < nbr.size(); ++j){
                    if(nbr[j]->dist(*sp->position) < model->h){
                        Vertex res = *sp->position - *sp->normal*((*nbr[j] - *sp->position)* *sp->normal);

                        float rad = ((*nbr[j] - *sp->position) - *sp->normal*((*nbr[j] - *sp->position) * *sp->normal)).dist();
                        
                        if(rad > radius){
                            radius = rad;
                        }

                    }
                }
                if(radius == 0.0){
                    radius = model->radius;
                }

                glNormal3f(sp->normal->x, sp->normal->y, sp->normal->z);
                Vertex point;
                float* retCoords = new float[2]; 
                for(float u = -radius; u <= radius; u+= 0.00007){
                    for(float v = -radius; v <= radius; v+= 0.00007){
                        if(u*u + v*v <= radius * radius){

                            retCoords[0] = u;
                            retCoords[1] = v;
                            getWorldCoords(retCoords, &point, *sp->position + *sp->position->normal * sp->position->t, *sp->normal, sp->position->params);
                            if(point.dist(*sp->position) <= radius)
                                glVertex3f(point.x * model->scale, point.y * model->scale, point.z * model->scale);
                        }
                    }
                }
            }
        }
}

void PSRWidget::drawSpheres(Sphere* sp, int* count){
        if(sp){
            if(sp->level == model->level)
                drawSphere(sp);
            if(sp->children && sp->level > model->level)
                {
                        
    
                    ++*count;
                    for(int i = 0; i < 4; ++i){
                        drawSpheres(sp->children[i], count);
                    }
                }   
        }
}

void PSRWidget::drawSphere(Sphere* sp){
    // std::cout << "Drawn sphere" << *sp->position << std::endl;
    // std::cout << sp->radius << std::endl;
    float radius = sp->radius;
    glPushMatrix();
    glTranslatef(sp->position->x* model->scale, sp->position->y* model->scale, sp->position->z* model->scale);
    glutSolidSphere(radius*model->scale, 10, 10);
    glPopMatrix();
    //uncomment to observe the normals of each sphere and comment 651-654
    // glNormal3f(sp->normal->x, sp->normal->y, sp->normal->z);
    // glVertex3f(sp->position->x * model->scale, sp->position->y* model->scale, sp->position->z* model->scale);
    // glVertex3f((sp->normal->x * 0.01 + sp->position->x) * model->scale, (sp->normal->y* 0.01  + sp->position->y) * model->scale, (sp->normal->z* 0.01  + sp->position->z)* model->scale);
    
}


void PSRWidget::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Q:
        model->angle -= 5;
        break; 
    case Qt::Key_E:
        model->angle += 5;
        break;

    case Qt::Key_Up:
        model->eye.y -= 1;
        break;
    case Qt::Key_Down:
        model->eye.y += 1;
        break;
    
    case Qt::Key_Left:
        model->eye.x += 1;
        break;
    case Qt::Key_Right:
        model->eye.x -= 1;
        break;
        
    case Qt::Key_W:
        model->eye.z += 1;
        break;
    case Qt::Key_S:
        model->eye.z -= 1;
        break;
    }
    model->viewDir = Vertex(Vertex(1,10,10) - model->eye);
    model->viewDir = model->viewDir.normalize();
    this->repaint();
}


void PSRWidget::exportPPM(){
    pixels = new GLubyte[w * h * 3];
    glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, pixels);
    std::ofstream outfile;
    unsigned int cur_pix;
    outfile.open("screen_shot_" + std::to_string(counter++) + ".ppm");

    outfile << "P3 " <<  w << ' ' << h << ' ' << 255 << '\n';

    for(size_t i = 0; i < h; ++i){
        for(size_t j = 0; j < w; ++j){
            cur_pix = 3 * ((h - i - 1) * w + j);
            outfile << ( int)pixels[cur_pix] << ' ' << ( int)pixels[cur_pix+1] << ' ' << ( int)pixels[cur_pix+2] << std::endl;
        }
    }
}