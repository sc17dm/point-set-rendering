#include "sphere.h"
#include <vector>

Sphere::Sphere(Vertex* pos, float rad, Vertex col, Vertex* norm, float* prms){
    position = pos;
    radius = rad;
    colour = col;
    normal = norm;
    params = prms;
    children = nullptr;
    level = 0;
}

Sphere::Sphere(Sphere* ch[4]){
    position = new Vertex();
    radius = 0.0f;
    colour = Vertex();
    normal = new Vertex();
    params = nullptr;
    children = new Sphere*[4];
    level = 0;
    for(int i = 0; i < 4; ++i){
        children[i] = ch[i];
    }

    int count = 0;
    float maxRad = 0.0, maxDist = 0.0;

    for(int i = 0; i < 4; ++i){
        if(children[i]){ 
            // std::cout << "child " << i << " radius: " << children[i]->radius << std::endl;
            if(children[i]->radius > maxRad)    
                maxRad = children[i]->radius;
            *position += *children[i]->position;
            *normal += *children[i]->normal;
            if(level < children[i]->level + 1){
                level  = children[i]->level + 1;
            }
            ++count; 
        }
    }

    *position *= 1.0/count;


    for(int i = 0; i < 4; ++i){
        if(children[i]){
            // std::cout << "child " << i << " dist: " << children[i]->position->dist(*position) << std::endl;
            if(children[i]->position->dist(*position) > maxDist)    
                maxDist = children[i]->position->dist(*position);
        }
    }
    radius = maxRad + maxDist;

    // std::cout << "Parent radius: " << radius << std::endl;
    
    *normal = normal->normalize();
}

Sphere* Sphere::buildTree(Vertex** verts, int begin, int end, float h){
    if(begin >= end){
        return nullptr;
    }
    if(begin == end - 1){
        return new Sphere(verts[begin], h, Vertex(0,1,0), verts[begin]->normal, nullptr);
    }
    else{
        int* indices = new int[3]();
        Sphere** child = new Sphere*[4];
        
        partitionAlongLongestAxis(verts, begin, end, indices);

        child[0] = buildTree(verts, begin, indices[0], h);
        child[1] = buildTree(verts, indices[0], indices[1], h);
        child[2] = buildTree(verts, indices[1], indices[2], h);
        child[3] = buildTree(verts, indices[2], end, h);
        
        return new Sphere(child);
    }
    return nullptr;
}


int maxi(float a, float b, float c, int choice){
    if(choice == 1){
        float temp = (a > b ? a : b);
        temp = (temp > c ? temp : c);
        if(temp == a)
            return 0;
        else if(temp == b)
            return 1;
        else return 2; 
    }
    else if(choice == 2){
        float temp = (a > b ? a : b);
        temp = (temp > c ? temp : c);

        if(temp == c){
            return (a > b ? 0 : 1);
        }
        else if(temp == b){
            return (a > c ? 0 : 2);
        }
        else if(temp == a){
            return (b > c ? 1 : 2);
        }
    }
    return -1;
}



void Sphere::partitionAlongLongestAxis(Vertex** verts, int begin, int end, int indices[4]){
    float xMax, yMax, zMax, xMin, yMin, zMin, *mid;
    mid = new float[3];
    int firstAxis, secondAxis;

    xMax = xMin = verts[begin]->x;
    yMax = yMin = verts[begin]->y;
    zMax = zMin = verts[begin]->z;

    for(int i = begin; i < end; ++i){
        if(verts[i]->x > xMax){
            xMax = verts[i]->x;
        }
        if(verts[i]->x < xMin){
            xMin = verts[i]->x;
        }

        if(verts[i]->y > yMax){
            yMax = verts[i]->y;
        }
        if(verts[i]->y < yMin){
            yMin = verts[i]->y;
        }

        if(verts[i]->z > zMax){
            zMax = verts[i]->z;
        }
        if(verts[i]->z < zMin){
            zMin = verts[i]->z;
        }
    }

    // std::cout << "min X: " << xMin << " max x: " << xMax << std::endl;
    // std::cout << "min y: " << yMin << " max y: " << yMax << std::endl;
    // std::cout << "min z: " << zMin << " max z: " << zMax << std::endl;

    mid[0] = xMin + (xMax - xMin)/2.0;
    mid[1] = yMin + (yMax - yMin)/2.0;
    mid[2] = zMin + (zMax - zMin)/2.0;

    // std::cout << "mid x: " << mid[0] << " mid y: " << mid[1] << " mid z:" << mid[2] << std::endl; 

    firstAxis = maxi(xMax - xMin,yMax - yMin, zMax - zMin, 1);

    secondAxis = maxi(xMax - xMin,yMax - yMin, zMax - zMin, 2);

    // std::cout << "first axis: " << firstAxis << " second axis: " << secondAxis << std::endl;

    std::vector<Vertex*> q1, q2, q3, q4;

    for(int i = begin; i < end; ++i){      
        if(((*verts[i])[firstAxis] <= mid[firstAxis]) && ((*verts[i])[secondAxis] <= mid[secondAxis])){
            q1.emplace_back(verts[i]);
        }
        else if(((*verts[i])[firstAxis] <= mid[firstAxis]) && ((*verts[i])[secondAxis] >= mid[secondAxis])){
            q2.emplace_back(verts[i]);
        }
        else if(((*verts[i])[firstAxis] >= mid[firstAxis]) && ((*verts[i])[secondAxis] <= mid[secondAxis])){
            q3.emplace_back(verts[i]);
        }
        else{
            q4.emplace_back(verts[i]);
        }
    }

    int counter = begin;

    for(size_t i = 0; i < q1.size(); ++i){
        verts[counter++] = q1[i];
    }

    for(size_t i = 0; i < q2.size(); ++i){
        verts[counter++] = q2[i];
    }

    for(size_t i = 0; i < q3.size(); ++i){
        verts[counter++] = q3[i];
    }

    for(size_t i = 0; i < q4.size(); ++i){
        verts[counter++] = q4[i];
    }

    indices[0] = begin + q1.size();
    indices[1] = begin + q1.size() + q2.size();
    indices[2] = begin + q1.size() + q2.size() + q3.size();
}
