#include "vertex.h"

Vertex::Vertex(){
    this->x = 0;
    this->y = 0;
    this->z = 0;
    normal = projection = nullptr;
    t = 0;
    shouldDraw = 0;
    priority = 0;
    upLevel = 0;
    isPP = false;
}

Vertex::Vertex(const Vertex &old){
    this->x = old.x;
    this->y = old.y;
    this->z = old.z;
    normal = old.normal;
    projection = old.projection;
    t = old.t;
    shouldDraw = old.shouldDraw;
    priority = old.priority;
    upLevel = old.upLevel;
    isPP = old.isPP;
}

Vertex::Vertex(float x, float y, float z){
    this->x = x;
    this->y = y;
    this->z = z;
    normal = projection = nullptr;
    t = 0;
    shouldDraw = 0;
    priority = 0;
    upLevel = 0;
    isPP = false;
}

bool Vertex::operator == (const Vertex &rhs) const{
    return (this->x == rhs.x && this->y == rhs.y && this->z == rhs.z);
}

Vertex Vertex::operator + (const Vertex &rhs) const{
    Vertex res(*this);

    res+=rhs;

    return res;
}

Vertex& Vertex::operator += (const Vertex &rhs){
    this->x += rhs.x;
    this->y += rhs.y;
    this->z += rhs.z;
    return *this;
}

Vertex Vertex::operator - () const{
    Vertex res(*this);
    res.x = -this->x;
    res.y = -this->y;
    res.z = -this->z;
    return res;
}

Vertex Vertex::operator - (const Vertex &rhs) const{
    Vertex res(*this);

    res-= rhs;

    return res;
}

Vertex& Vertex::operator -= (const Vertex &rhs){
    this->x -= rhs.x;
    this->y -= rhs.y;
    this->z -= rhs.z;
    return *this;
}

float Vertex::operator * (const Vertex &rhs) const{
    float res;

    res = this->x * rhs.x;
    res += this->y * rhs.y;
    res += this->z * rhs.z;

    return res;
}

Vertex Vertex::operator * (float const &rhs) const{
    Vertex res(*this);
    res *= rhs;
    return res;
}

Vertex& Vertex::operator *= (float const &rhs){
    this->x *= rhs;
    this->y *= rhs;
    this->z *= rhs;
    return *this;
}

float& Vertex::operator [](int index){
    switch(index){
        case 0: return *&this->x;
        case 1: return *&this->y;
        case 2: return *&this->z;
    }
}

float Vertex::dist(const Vertex &rhs) const{
    return sqrt(((*this) - rhs) * ((*this) - rhs));
}

float Vertex::dist() const{
    return sqrt((*this) * (*this));
}

Vertex Vertex::cross(const Vertex &rhs) const{
    Vertex res;

    res.x = rhs.y * this->z - rhs.z * this->y;
    res.y = rhs.z * this->x - rhs.x * this->z;
    res.z = rhs.x * this->y - rhs.y * this->x;

    return res;
}
        
Vertex Vertex::cross() const{
    Vertex res;

    res = this->cross(*this);

    return res;
}

Vertex Vertex::normalize() const{
    Vertex res(*this);

    res *= 1.0/res.dist();

    return res;
}

std::ostream& operator<<(std::ostream& ostr, const Vertex& rhs){
    ostr << '(' << rhs.x << ", " << rhs.y << ", " << rhs.z << ')'; 
    return ostr;
}