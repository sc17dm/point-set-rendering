#include "octree.h"
#include "grid.h"


Octree::Octree(){
    centroid = new Vertex();
    children = new Octree*[8];
    for(int i = 0; i < 8; ++i){
        children[i] = nullptr;
    }
    point = nullptr;
    pointCount = 0;
    position = Vertex();
    size = 0;
    hasPoint = 0;
    isRetVal = 0;
}

Octree::Octree(Vertex position, float size){
    centroid = new Vertex();
    children = new Octree*[8];
    for(int i = 0; i < 8; ++i){
        children[i] = nullptr;
    }
    point = nullptr;
    pointCount = 0;
    this->position = position;
    this->size = size;
    hasPoint = 0;
    isRetVal = 0;
}

void Octree::addVertex(Vertex* vert){
    if(vert == nullptr)
        return;
    *centroid += *vert; 
    ++pointCount;

    if(!hasPoint){
        this->createChildren();
        point = vert;
        hasPoint = 1;
    }
    else{

        Vertex* corner = new Vertex();
        int index;
        float newSize = size/2.0;
        computeCoords(corner, &index, *vert, newSize);
        
        // std::cout << "Adding first vertex" << *vert << " at index " << index <<  std::endl;
        // std::cout << "current point:" << point << std::endl;
        children[index]->addVertex(vert);
        
        if(point == nullptr){
            return;
        }

        computeCoords(corner, &index, *point, newSize);        
        // std::cout << "Adding second vertex" << *point << " at index " << index <<  std::endl;
        // std::cout << "current point:" <<point << std::endl;
        children[index]->addVertex(point);

        point = nullptr;
    }
    // std::cout << "___________________\n";
}

// accepts the size of the cell that has to be computed (i.e. size of parent / 2)
void Octree::computeCoords(Vertex* corner, int* index, Vertex vert, float size){
    *corner = position;

    // std::cout << "children size: " << size << std::endl;
    // std::cout << "corner: " << *corner << std::endl;

    Vertex positioning = vert - position;

    if(size){
        positioning*=1.0f/size;
    }
    else
    {
        positioning = Vertex(0,0,0);
    }
    

    if(positioning.x >= 2)
        --positioning.x;
    if(positioning.y >= 2)
        --positioning.y;
    if(positioning.z >= 2)
        --positioning.z;

    // std::cout << positioning << std::endl;

    *index = ((int)positioning.z << 2) + ((int)positioning.y << 1) + (int)positioning.x; 

    corner->x += positioning.x*size;
    corner->y += positioning.y*size;
    corner->z += positioning.z*size; 
}

void Octree::createChildren(){
    int x,y,z;
    Vertex childPos = this->position;
    float newSize = this->size/2.0;
    for(int i = 0; i < 8; ++i){
        x = i%2;
        y = i/2%2;
        z = i/4%2;
        childPos.x += x*newSize;
        childPos.y += y*newSize;
        childPos.z += z*newSize;
        children[i] = new Octree(childPos, newSize);
        childPos = this->position;
    }
}

