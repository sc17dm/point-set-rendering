#include "controller.h"
#include <iostream>

Controller::Controller(Model *newModel, PSRWindow *newView){
    model = newModel;
    view = newView;

    connect(view->hSlider, &QSlider::valueChanged, this, &Controller::hChanged);
    connect(view->radiusSlider, &QSlider::valueChanged, this, &Controller::radiusChanged);
    connect(view->scaleSlider, &QSlider::valueChanged, this, &Controller::scaleChanged);
    connect(view->levelSlider, &QSlider::valueChanged, this, &Controller::levelChanged);
    connect(view->downSlider, &QSlider::valueChanged, this, &Controller::downChanged);
    connect(view->upSlider, &QSlider::valueChanged, this, &Controller::upChanged);
    connect(view->renderPointButton, &QCheckBox::stateChanged, this, &Controller::pointChanged);
    connect(view->renderProjPointButton, &QCheckBox::stateChanged, this, &Controller::projPointChanged);
    connect(view->renderRepresentationPointButton, &QCheckBox::stateChanged, this, &Controller::representationChanged);
    connect(view->renderOctreeButton, &QCheckBox::stateChanged, this, &Controller::octreeChanged);
    connect(view->renderNormalButton, &QCheckBox::stateChanged, this, &Controller::normalChanged);
    connect(view->renderCompNormalButton, &QCheckBox::stateChanged, this, &Controller::compNormalChanged);
    connect(view->renderSurfaceButton, &QCheckBox::stateChanged, this, &Controller::surfaceChanged);
    connect(view->renderSpheresButton, &QCheckBox::stateChanged, this, &Controller::spheresChanged);
    connect(view->projectSurfButton, &QPushButton::pressed,this, &Controller::projectSurfaceChanged);
    connect(view->exportPPMButton, &QPushButton::pressed,this, &Controller::exportPPMChanged);
}

void Controller::scaleChanged(int newValue){
    model->scale = newValue;
    view->ResetInterface();
}

void Controller::levelChanged(int newValue){
    model->level = newValue;
    view->ResetInterface();
}

void Controller::downChanged(int newValue){
    model->downSample = newValue;
    view->ResetInterface();
}

void Controller::upChanged(int newValue){
    model->upSample = newValue;
    view->ResetInterface();
}

void Controller::pointChanged(int newValue){
    model->shouldRenderPoints = newValue;
    view->ResetInterface();
}

void Controller::projPointChanged(int newValue){
    model->shouldRenderProjPoints = newValue;
    view->ResetInterface();
}

void Controller::octreeChanged(int newValue){
    model->shouldRenderOctrees = newValue;
    view->ResetInterface();
}

void Controller::representationChanged(int newValue){
    model->shouldRenderRepresentationPoints = newValue;
    view->ResetInterface();
}

void Controller::normalChanged(int newValue){
    model->shouldRenderNormals = newValue;
    view->ResetInterface();
}

void Controller::compNormalChanged(int newValue){
    model->shouldRenderCompNormals = newValue;
    view->ResetInterface();
}

void Controller::surfaceChanged(int newValue){
    model->shouldRenderSurface = newValue;
    view->ResetInterface();
}

void Controller::spheresChanged(int newValue){
    model->shouldRenderSpheres = newValue;
    view->ResetInterface();
}

void Controller::projectSurfaceChanged(){
    auto start = std::chrono::high_resolution_clock::now();
    projectSurface(model->h, model->n, model->verts, sigma, model->epsilon, polynomial3, &model->partitionGrid);
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start); 
    std::cout << "Total duration: " << duration.count() << std::endl;
    view->ResetInterface();
}

void Controller::exportPPMChanged(){
    view->psrWidget->exportPPM();
}

void Controller::hChanged(int newValue){
    model->h = (float)newValue/10000.0;
    view->ResetInterface();
}

void Controller::radiusChanged(int newValue){
    model->radius = (float)newValue/100000.0;
    view->ResetInterface();
}