#include "grid.h"

Grid::Grid(){
    cells = nullptr;
    xSize = ySize = zSize = 0;
    neglDist = 0;;
    retVal = new Octree();
    retVal->isRetVal = 1;
}

Grid::Grid(float x, float y, float z, float NeglDist, float XMin, float YMin, float ZMin){
    neglDist = NeglDist;
    x = ceil(x/neglDist);
    y = ceil(y/neglDist);
    z = ceil(z/neglDist);
    xSize = x;
    ySize = y;
    zSize = z;
    xMin = XMin;
    yMin = YMin;
    zMin = ZMin;
    cells = new Octree*[xSize*ySize*zSize]();
    for(int i = 0; i < xSize; ++i){
        for(int j = 0; j < ySize; ++j){
            for(int k = 0; k < zSize; ++k){
                (*this)(i,j,k) = new Octree(Vertex(i*neglDist + xMin, j*neglDist + yMin, k*neglDist + zMin), neglDist);
                // std::cout << "Initialising octree at index " << i << j << k <<" with " << (*this)(i,j,k) << std::endl;
            }
        }
    }

    retVal = new Octree();
    retVal->isRetVal = 1;
    
    // std::cout << "Size: " << xSize * ySize * zSize << std::endl;
    
}

Octree*& Grid::operator() (int x, int y, int z){
    // std::cout << "x: " << x << ", y: " << y << ", z: " << z << std::endl;
    // std::cout << "index: " << x + y * xSize + z * xSize * ySize << std::endl;
    if(x < 0 || x >= xSize || y < 0 || y >= ySize || z < 0 || z >= zSize)
        return retVal;
    
    return cells[x + y * xSize + z * xSize * ySize];
}



std::ostream& operator<<(std::ostream& ostr, Grid& rhs){
    for(int i = 0; i < rhs.xSize; ++i){
        for(int j = 0;  j < rhs.ySize; ++j){
            for(int k = 0; k < rhs.zSize; ++k){
                ostr << "Cell at (" << i << ' ' << j << ' ' << k << "):" << std::endl;
                ostr << std::endl;
            }
        }
    }

    return ostr;
}