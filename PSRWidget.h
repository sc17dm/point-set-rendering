#pragma once

#include <QGLWidget>
#include <QKeyEvent>
#include <GL/glu.h>
#include "model.h"
#include <GL/glut.h>
#include <fstream>

struct material{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
};



class PSRWidget : public QGLWidget{

    Q_OBJECT

    public:
        PSRWidget(Model* model, QWidget* parent);
        
        Model* model;
        unsigned int w;
        unsigned int h;
        GLubyte *pixels;
        int counter;

        void loadVertices();
        void drawSpheres(Sphere* sp, int* count);
        void drawSphere(Sphere* sp);
        void drawOctree(Octree* oct);
        void drawSurface(Sphere* sp);
        void exportPPM();
        void renderOriginalSurf();
        void getNormal(Vertex* normal, Vertex* index);
        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);
        void keyPressEvent(QKeyEvent *event);
};