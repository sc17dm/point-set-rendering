#include <fstream>
#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char *argv[]){

    ifstream in1;
    in1.open(argv[1]);
    ifstream in2;
    in2.open(argv[2]);
    ofstream out;
    out.open("buddha_results.ppm");

    string temp;
    int w,h,c;

    in1 >> temp;
    in2 >> temp;

    out << temp;

    in1 >> w >> h >> c;
    in2 >> w >> h >> c;

    out << ' ' << w << ' ' << h <<  ' ' << c << std::endl;

    int r1,g1,b1,r2,g2,b2;
    float* diff = new float[w*h], mean = 0, sdiv = 0;

    for(int i = 0; i < w*h; ++i){
        in1>> r1 >> g1 >> b1;
        in2>> r2 >> g2 >> b2;

        diff[i] =(abs(r2-r1) + abs(g2-g1) + abs(b2-b1))/3.0;

        mean += diff[i];

        out << (r2+(int)diff[i]>255?255:r2+(int)diff[i])   << ' ' << g2 << ' ' << b2  << std::endl;
    }

    mean/=(w*h);

    for(int i = 0; i < w*h; ++i){
        sdiv += (diff[i] - mean) * (diff[i] - mean); 
    }

    sdiv/=(w*h-1);

    sdiv = sqrt(sdiv);


    cout << "Mean: " << mean << " Standard dev.: " << sdiv <<  std::endl;
}