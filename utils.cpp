#include "utils.h"


Vertex pcom, xicom;
float (*nrfunc)(Vertex);
void (*nrdfunc)(Vertex, Vertex*);
std::vector<Vertex*>* global_verts;
int global_N;
float global_h;
Vertex global_normal;

float dbrent(float bracketA, float bracketB, float bracketC, float (*function)(float), float (*derivative)(float), float tol, float* xmin){
    int iter, ok1, ok2;
    float a, b, d, d1, d2, du, dv, dw, dx, e = 0.0;
    float fu, fv, fw, fx, olde, tol1, tol2, u, u1, u2, v, w, x, xm;

    a = (bracketA < bracketC ? bracketA : bracketC);
    b = (bracketA > bracketC ? bracketA : bracketC);

    x = w = v = bracketB;

    fw = fv = fx = (*function)(x);
    dw = dv = dx = (*derivative)(x);

    for(iter = 1; iter <= ITMAX; ++iter){
        xm = 0.5 * (a + b);
        tol1 = tol * fabs(x) + ZEPS;
        tol2 = 2.0 * tol1;
        if(fabs(x - xm) <= (tol2 - 0.5 * (b-a))){
            *xmin = x;
            return fx;
        }
        if(fabs(e) > tol1){
            d1 = 2.0 * (b - a);
            d2 = d1;

            if(dw != dx){
                d1 = (w - x) * dx / (dx - dw);
            }
            
            if(dv != dx){
                d2 = (v - x) * dx / (dx - dv);
            }
            
            u1 = x + d1;
            u2 = x + d2;

            ok1 = (a - u1) * (u1 - b) > 0.0 && dx * d1 <= 0.0;
            ok2 = (a - u2) * (u2 - b) > 0.0 && dx * d2 <= 0.0;            

            olde = e;
            e = d;

            if(ok1 || ok2){
                if(ok1 && ok2){
                    d = (fabs(d1) < fabs(d2) ? d1 : d2);
                }
                else if(ok1){
                    d = d1;
                }
                else{
                    d = d2;
                }

                if(fabs(d) <= fabs(0.5 * olde)){
                    u = x + d;
                    if(u - a < tol2 || b-u < tol2){
                        d = SIGN(tol1, xm - x);
                    }
                }
                else{
                    e = (dx >= 0.0 ? a-x : b-x);
                    d = 0.5 * e;
                }
            }
            else{
                e = (dx >= 0.0 ? a-x : b-x);
                d = 0.5 * e;
            }
        }
        else{
            e = (dx >= 0.0 ? a-x : b-x);
            d = 0.5 * e;
        }

        if(fabs(d) >= tol1){
            u = x + d;
            fu = (*function)(x);
        }
        else{
            u = x + SIGN(tol1, d);
            fu = (*function)(u);

            if(fu > fx){
                *xmin = x;
                return fx;
            }
        }

        du = (*derivative)(u);

        if(fu <= fx){
            if(u >= x){
                a = x;
            }
            else{
                b = x;
            }
            MOV3(v, fv, dv, w, fw, dw);
            MOV3(w, fw, dw, x, fx, dx);
            MOV3(x, fx, dx, u, fu, du);
        }
        else{
            if(u < x){
                a = u;
            }
            else{
                b = u;
            }

            if(fu <= fw || w == x){
                MOV3(v, fv, dv, w, fw, dw);
                MOV3(w, fw, dw, u, fu, du);
            }
            else if(fu < fv || v == x || v == w){
                MOV3(v, fv, dv, u, fu, du);
            }
        }
    }
    std::cerr << "Too many iterations in original dbrent" << std::endl;
    return 0.0;
}

float dbrent(float bracketA, float bracketB, float bracketC, float tol, float* xmin, int N, Vertex** verts, float(*radWeight)(float, float), Vertex normal, Vertex point, float h){
    int iter, ok1, ok2;
    float a, b, d, d1, d2, du, dv, dw, dx, e = 0.0;
    float fu, fv, fw, fx, olde, tol1, tol2, u, u1, u2, v, w, x, xm;

    a = (bracketA < bracketC ? bracketA : bracketC);
    b = (bracketA > bracketC ? bracketA : bracketC);

    x = w = v = bracketB;

    fw = fv = fx = mlsFunction(N, verts, radWeight, x, normal, point, h);
    dw = dv = dx = mlsDerivative(N, verts, radWeight, x, normal, point, h);

    for(iter = 1; iter <= ITMAX; ++iter){
        xm = 0.5 * (a + b);
        tol1 = tol * fabs(x) + ZEPS;
        tol2 = 2.0 * tol1;
        if(fabs(x - xm) <= (tol2 - 0.5 * (b-a))){
            *xmin = x;
            return fx;
        }
        if(fabs(e) > tol1){
            d1 = 2.0 * (b - a);
            d2 = d1;

            if(dw != dx){
                d1 = (w - x) * dx / (dx - dw);
            }
            
            if(dv != dx){
                d2 = (v - x) * dx / (dx - dv);
            }
            
            u1 = x + d1;
            u2 = x + d2;

            ok1 = (a - u1) * (u1 - b) > 0.0 && dx * d1 <= 0.0;
            ok2 = (a - u2) * (u2 - b) > 0.0 && dx * d2 <= 0.0;            

            olde = e;
            e = d;

            if(ok1 || ok2){
                if(ok1 && ok2){
                    d = (fabs(d1) < fabs(d2) ? d1 : d2);
                }
                else if(ok1){
                    d = d1;
                }
                else{
                    d = d2;
                }

                if(fabs(d) <= fabs(0.5 * olde)){
                    u = x + d;
                    if(u - a < tol2 || b-u < tol2){
                        d = SIGN(tol1, xm - x);
                    }
                }
                else{
                    e = (dx >= 0.0 ? a-x : b-x);
                    d = 0.5 * e;
                }
            }
            else{
                e = (dx >= 0.0 ? a-x : b-x);
                d = 0.5 * e;
            }
        }
        else{
            e = (dx >= 0.0 ? a-x : b-x);
            d = 0.5 * e;
        }

        if(fabs(d) >= tol1){
            u = x + d;
            fu = mlsFunction(N, verts, radWeight, x, normal, point, h);
        }
        else{
            u = x + SIGN(tol1, d);
            fu = mlsFunction(N, verts, radWeight, u, normal, point, h);

            if(fu > fx){
                *xmin = x;
                return fx;
            }
        }

        du = mlsDerivative(N, verts, radWeight, x, normal, point, h);

        if(fu <= fx){
            if(u >= x){
                a = x;
            }
            else{
                b = x;
            }
            MOV3(v, fv, dv, w, fw, dw);
            MOV3(w, fw, dw, x, fx, dx);
            MOV3(x, fx, dx, u, fu, du);
        }
        else{
            if(u < x){
                a = u;
            }
            else{
                b = u;
            }

            if(fu <= fw || w == x){
                MOV3(v, fv, dv, w, fw, dw);
                MOV3(w, fw, dw, u, fu, du);
            }
            else if(fu < fv || v == x || v == w){
                MOV3(v, fv, dv, u, fu, du);
            }
        }
    }
    std::cerr << "Too many iterations in mls dbrent" << std::endl;
    return 0.0;
}


float dbrent(float bracketA, float bracketB, float bracketC, float tol, float* xmin, int N, std::vector<Vertex*> verts, float(*radWeight)(float, float), Vertex normal, Vertex point, float h){
    int iter, ok1, ok2;
    float a, b, d, d1, d2, du, dv, dw, dx, e = 0.0;
    float fu, fv, fw, fx, olde, tol1, tol2, u, u1, u2, v, w, x, xm;

    a = (bracketA < bracketC ? bracketA : bracketC);
    b = (bracketA > bracketC ? bracketA : bracketC);

    x = w = v = bracketB;

    fw = fv = fx = mlsFunction(N, verts, radWeight, x, normal, point, h);
    dw = dv = dx = mlsDerivative(N, verts, radWeight, x, normal, point, h);

    for(iter = 1; iter <= ITMAX; ++iter){
        xm = 0.5 * (a + b);
        tol1 = tol * fabs(x) + ZEPS;
        tol2 = 2.0 * tol1;
        if(fabs(x - xm) <= (tol2 - 0.5 * (b-a))){
            *xmin = x;
            return fx;
        }
        if(fabs(e) > tol1){
            d1 = 2.0 * (b - a);
            d2 = d1;

            if(dw != dx){
                d1 = (w - x) * dx / (dx - dw);
            }
            
            if(dv != dx){
                d2 = (v - x) * dx / (dx - dv);
            }
            
            u1 = x + d1;
            u2 = x + d2;

            ok1 = (a - u1) * (u1 - b) > 0.0 && dx * d1 <= 0.0;
            ok2 = (a - u2) * (u2 - b) > 0.0 && dx * d2 <= 0.0;            

            olde = e;
            e = d;

            if(ok1 || ok2){
                if(ok1 && ok2){
                    d = (fabs(d1) < fabs(d2) ? d1 : d2);
                }
                else if(ok1){
                    d = d1;
                }
                else{
                    d = d2;
                }

                if(fabs(d) <= fabs(0.5 * olde)){
                    u = x + d;
                    if(u - a < tol2 || b-u < tol2){
                        d = SIGN(tol1, xm - x);
                    }
                }
                else{
                    e = (dx >= 0.0 ? a-x : b-x);
                    d = 0.5 * e;
                }
            }
            else{
                e = (dx >= 0.0 ? a-x : b-x);
                d = 0.5 * e;
            }
        }
        else{
            e = (dx >= 0.0 ? a-x : b-x);
            d = 0.5 * e;
        }

        if(fabs(d) >= tol1){
            u = x + d;
            fu = mlsFunction(N, verts, radWeight, x, normal, point, h);
        }
        else{
            u = x + SIGN(tol1, d);
            fu = mlsFunction(N, verts, radWeight, u, normal, point, h);

            if(fu > fx){
                *xmin = x;
                return fx;
            }
        }

        du = mlsDerivative(N, verts, radWeight, x, normal, point, h);

        if(fu <= fx){
            if(u >= x){
                a = x;
            }
            else{
                b = x;
            }
            MOV3(v, fv, dv, w, fw, dw);
            MOV3(w, fw, dw, x, fx, dx);
            MOV3(x, fx, dx, u, fu, du);
        }
        else{
            if(u < x){
                a = u;
            }
            else{
                b = u;
            }

            if(fu <= fw || w == x){
                MOV3(v, fv, dv, w, fw, dw);
                MOV3(w, fw, dw, u, fu, du);
            }
            else if(fu < fv || v == x || v == w){
                MOV3(v, fv, dv, u, fu, du);
            }
        }
    }
    std::cerr << "Too many iterations in mls dbrent" << std::endl;
    return 0.0;
}


void conjGradient(Vertex *p, float ftol, float *fret, std::vector<Vertex*> verts, int N, Vertex normal, float(*radWeight)(float,float), float h_rad){
    int its;
    float gg, gam, fp, dgg;
    Vertex g, h, *xi;
    xi = new Vertex();
    global_N = N;
    global_verts = &verts;
    global_normal = normal;
    global_h = h_rad;

    fp = qFuncCG(verts, N, *p, normal, radWeight, h_rad);
    qDerivCG(verts, N, *p, xi, normal, radWeight, h_rad);


    g = -*xi;
    h = g;
    *xi = h;

    for(its = 1; its <= ITMAX; ++its){

        dlinmin(p,xi,fret);

        if(2.0* fabs(*fret - fp) <= ftol *(fabs(*fret) + fabs(fp) + EPS)){
            return;
        }

        fp = *fret;
        qDerivCG(verts, N, *p, xi, normal, radWeight, h_rad);

        dgg = gg = 0.0;

        gg += g.x * g.x;
        gg += g.y * g.y;
        gg += g.z * g.z;

        dgg += (xi->x + g.x) * xi->x;
        dgg += (xi->y + g.y) * xi->y;
        dgg += (xi->z + g.z) * xi->z;

        if(gg == 0.0){
            return;
        }

        gam = dgg / gg;

        g = -*xi;
        *xi = h = g + h*gam;
    }
    std::cerr << "Too many iterations in conj gradient";
}

void dlinmin(Vertex* p, Vertex* xi, float *fret, float (*func)(Vertex), void (*dfunc)(Vertex, Vertex*)){
    float xx, xmin, fx, fb, fa, bx, ax;

    nrfunc = func;
    nrdfunc = dfunc;
    pcom = *p;
    xicom = *xi;

    ax = 0.0;
    xx = 1.0;

    mnbrak(&ax, &xx, &bx, &fa, &fx, &fb, f1dim);

    *fret = dbrent(ax, xx, bx, f1dim, df1dim, TOL, &xmin);

    *xi *= xmin;
    *p += *xi;
}

void dlinmin(Vertex* p, Vertex* xi, float *fret){
    float xx, xmin, fx, fb, fa, bx, ax;

    pcom = *p;
    xicom = *xi;

    ax = 0.0;
    xx = 1.0;

    mnbrak(&ax, &xx, &bx, &fa, &fx, &fb, f1dim);

    *fret = dbrent(ax, xx, bx, f1dim, df1dim, TOL, &xmin);

    *xi *= xmin;
    *p += *xi;
}

float sigma(float d, float h){
    return exp(-(d*d)/(h*h));
}

float mlsFunction(int N, Vertex** verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h){
    float res = 0.0;
    
    for(int i = 0; i < N; ++i){
        res += (normal * (*verts[i] - point - normal * t)) * (normal * (*verts[i] - point - normal * t)) * radWeight((*verts[i] - point - normal * t).dist(), h);
    }

    return res;
}

float mlsFunction(int N, std::vector<Vertex*> verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h){
    float res = 0.0;
    
    for(int i = 0; i < N; ++i){
        res += (normal * (*verts[i] - point - normal * t)) * (normal * (*verts[i] - point - normal * t)) * radWeight((*verts[i] - point - normal * t).dist(), h);
    }

    return res;
}

float mlsDerivative(int N, std::vector<Vertex*> verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h){
    float res = 0.0;
    
    for(int i = 0; i < N; ++i){
        res += 2 * (normal * (*verts[i] - point - normal * t)) * (1 + ((normal *(*verts[i] - point - normal * t)) * (normal *(*verts[i] - point - normal * t)))/h/h) * radWeight(-(*verts[i] - point - normal * t).dist(), h); 
    }

    return res;
}

float mlsDerivative(int N, Vertex** verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h){
    float res = 0.0;
    
    for(int i = 0; i < N; ++i){
        res += 2 * (normal * (*verts[i] - point - normal * t)) * (1 + (normal *(*verts[i] - point - normal * t) * (normal *(*verts[i] - point - normal * t)))/h/h) * radWeight(-(*verts[i] - point - normal * t).dist(), h); 
    }

    return res;
}

float qFunction(int N, Vertex** verts, Vertex** projections, float(*radWeight)(float, float), float (*polynomial)(float*, float, float), float *params, Vertex normal, Vertex q, float h){
    float res = 0.0;
    float distance, *coords;

    coords = new float[2];

    for(int i = 0; i < N; ++i){
        distance = normal * (*verts[i] - q);
        getLocalCoords(coords, *projections[i], q, normal);
        res += (polynomial(params, coords[0], coords[1]) - distance) * (polynomial(params, coords[0], coords[1]) - distance) * radWeight(verts[i]->dist(q), h); 
    }
    return res;
}

float qDerivative(int N, Vertex** verts, Vertex** projections, float(*radWeight)(float, float), float (*polynomial)(float*, float, float), float *params, Vertex normal, Vertex q, float h){
    float res = 0.0;
    float distance, *coords;

    coords = new float[2];

    for(int i = 0; i < N; ++i){
        distance = normal * (*verts[i] - q);
        getLocalCoords(coords, *projections[i], q, normal);
        res += (polynomial(params, coords[0], coords[1]) - distance) * (polynomial(params, coords[0], coords[1]) - distance) * radWeight(verts[i]->dist(q), h) * 2 / h / h * (*verts[i] - q).dist(); 
    }
    return res;
}


void getLocalCoords(float* returnCoords, Vertex point, Vertex origin, Vertex normal){
    Vertex xAxis, zAxis, proj;

    returnCoords[0] = returnCoords[1] = 0;



    point -= origin;
    xAxis = Vertex(-normal.z, 0, normal.x);

    if(xAxis == Vertex(0,0,0)){
        xAxis = Vertex(normal.y, 0, 0);
    }

    // std::cout << "e1 before normalisation: " << xAxis << std::endl;
    xAxis = xAxis.normalize();
    // std::cout << "e1 after normalisation: " << xAxis << std::endl;

    normal = normal.normalize();
    
    zAxis = xAxis.cross(normal);
    // std::cout << "e2 before normalisation: " << zAxis << std::endl;

    zAxis = zAxis.normalize();
    // std::cout << "e2 after normalisation: " << zAxis << std::endl;

    // std::cout << "dot prod: " << point * normal << std::endl;



    proj = point - normal * (point * normal); // the projection of the point on the plan

    
    // std::cout << "projection: " << proj << std::endl;   

    returnCoords[0] = xAxis * proj;
    returnCoords[1] = zAxis * proj; 
}

void getWorldCoords(float* returnCoords, Vertex* point, Vertex origin, Vertex normal, float* params){
    Vertex xAxis, zAxis, proj;



    
    xAxis = Vertex(-normal.z, 0, normal.x);
    
    if(xAxis == Vertex(0,0,0)){
        xAxis = Vertex(normal.y, 0, 0);
    }
    
    // std::cout << "e1 before normalisation: " << xAxis << std::endl;
    xAxis = xAxis.normalize();
    // std::cout << "e1 after normalization: " << xAxis << std::endl;
    zAxis = xAxis.cross(normal);
    // std::cout << "e2 before normalisation: " << zAxis << std::endl;
    zAxis = zAxis.normalize();

    // std::cout << "e2 after normalisation: " << zAxis << std::endl;
    
    // std::cout << "dot prod: " << point * normal << std::endl;
    
    proj = xAxis*returnCoords[0] + zAxis * returnCoords[1];
    
    // std::cout << "projection: " << proj << std::endl;

    *point = origin + proj + normal * polynomial3(params, returnCoords[0], returnCoords[1]); // the projection of the point on the plane
    
    // std::cout << "point: " << *point << std::endl;
}

float polynomial3(float* params, float x, float y){
    return params[0]* x * x * x + params[1] * x * x * y + params[2] * x * y * y + params[3] * y * y * y + params[4] * x * x + params[5] * x * y + params[6] * y * y + params[7] * x + params[8] * y + params[9]; 
}

float df1dim(float x){
    float df1 = 0.0;
    Vertex xt, *df;

    df = new Vertex();

    xt = pcom + xicom * x;

    qDerivCG(*global_verts, global_N, xt, df, global_normal, sigma, global_h);

    df1 += *df * xicom;
    
    return df1;
}

float f1dim(float x){
    float f;
    Vertex xt;

    xt = pcom + xicom * x;
    f = qFuncCG(*global_verts,global_N,xt,global_normal, sigma, global_h);
    
    return f;
}

void mnbrak(float* ax, float *bx, float *cx, float *fa, float *fb, float *fc, float (*func)(float)){
    float ulim, u, r, q, fu, dum;

    *fa = (*func)(*ax);
    *fb = (*func)(*bx);

    if(*fb > *fa){
        SHIFT(dum,*ax,*bx,dum)
        SHIFT(dum,*fb,*fa,dum)
    }

    *cx = (*bx) + GOLD * (*bx - *ax);
    *fc = (*func)(*cx);

    while(*fb > *fc){
        r = (*bx - *ax) * (*fb - *fc);
        q = (*bx - *cx) * (*fb - *fa);
        u = (*bx) - ((*bx - *cx) * q - (*bx - *ax) * r)/(2.0 * SIGN(FMAX(fabs(q-r), TINY), q-r));

        ulim = (*bx) + GLIMIT*(*cx-*bx);

        if((*bx - u) * (u - *cx) > 0.0){
            fu = (*func)(u);
            if(fu < *fc){
                *ax = (*bx);
                *bx = u;
                *fa = (*fb);
                *fb = fu;
                return;
            }
            else if(fu > *fb){
                *cx = u;
                *fc = fu;
                return;
            }
            
            u = (*cx) + GOLD * (*cx - *bx);
            fu = (*func)(u);
        }
        else if((*cx - u) * (u - ulim) > 0.0){
            fu = (*func)(u);
            if(fu < *fc){
                SHIFT(*bx,*cx,u,*cx+GOLD*(*cx-*bx))
                SHIFT(*fb,*fc,fu,(*func)(u))
            }
        }
        else if ((u - ulim) * (ulim - *cx) >= 0.0){
            u = ulim;
            fu = (*func)(u);
        }
        else{
            u = (*cx) + GOLD * (*cx - *bx);
            fu = (*func)(u);
        }
        SHIFT(*ax,*bx,*cx,u)
        SHIFT(*fa,*fb,*fc,fu)
    }
}

float funcCoeff(int coeff, float x, float y){
    switch(coeff){
        case 0: return x * x * x;
        case 1: return x * x * y;
        case 2: return x * y * y;
        case 3: return y * y * y;
        case 4: return x * x;
        case 5: return x * y;
        case 6: return y * y;
        case 7: return x;
        case 8: return y;
        case 9: return 1.0;
        default: return 0.0;
    }
}

void GaussJ(float** matrix, int n, float* b){
    int i, icol, irow, j, k, l, ll;
    float big, dum, pivinv;
    int* indxc, *indxr, *ipiv;
    indxc = new int[n];
    indxr = new int[n];
    ipiv = new int[n];

    for(j = 0; j < n; ++j){
        ipiv[j] = 0;
    }

    for(i = 0; i < n; ++i){
        big = 0.0;
        for(j = 0; j < n; ++j){
            if(ipiv[j] != 1){
                for(k = 0; k < n; ++k){
                    if(ipiv[k] == 0){
                        if(fabs(matrix[j][k]) >= big){
                            big = fabs(matrix[j][k]);
                            irow = j;
                            icol = k;
                        }
                    }
                }
            }
        }
        ++(ipiv[icol]);

        if(irow != icol){
            for(l = 0; l < n; ++l){
                float temp = matrix[irow][l];
                matrix[irow][l] = matrix[icol][l];
                matrix[icol][l] = temp;
            }

            for(l = 0; l < 1; ++l){
                float temp = b[irow];
                b[irow] = b[icol];
                b[icol] = temp;
            }
        }

        indxr[i] = irow;
        indxc[i] = icol;

        if(!matrix[icol][icol]){
            std::cerr << "Singular matrix in Gauss" << std::endl;
        }

        pivinv = 1.0/matrix[icol][icol];

        matrix[icol][icol] = 1.0;

        for(l = 0; l < n; ++l){
            matrix[icol][l] *= pivinv;
        }

        b[icol] *= pivinv;

        for(ll = 0; ll < n; ++ll){
            if(ll != icol){
                dum = matrix[ll][icol];
                matrix[ll][icol] = 0.0;
                for(l = 0; l < n; ++l){
                    matrix[ll][l] -= matrix[icol][l]*dum;
                }
                b[ll] -= b[icol]*dum;
            }
        }
    }
}


void LUDecomposition(float** matrix, int n, int* index){
    int i, imax, j, k;
    float big, dum, sum, temp;
    float *vv;

    vv = new float[n];

    for(i = 0; i < n; ++i){
        big = 0.0;
        for(j = 0; j < n; ++j){
            if((temp = fabs(matrix[i][j])) > big){
                big = temp;
            }
        }

        if(big == 0.0){
            std::cerr << "Singular matrix in routine LUDecomposition" << std::endl;
        }

        vv[i] = 1.0/big;
    }

    for(j = 0; j < n; ++j){
        for(i = 0; i < j; ++i){
            sum = matrix[i][j];
            for(k = 0; k < i; ++k){
                sum -= matrix[i][k] * matrix[k][j];
            }
            matrix[i][j] = sum;
        }
        big = 0.0;

        for(i = j; i < n; ++i){
            sum = matrix[i][j];
            for(k = 0; k < j; ++k){
                sum -= matrix[i][k] * matrix[k][j];
            }
            matrix[i][j] = sum;

            if((dum = vv[i] * fabs(sum)) >= big){
                big = dum;
                imax = i;
            }
        }

        if(j != imax){
            for(k = 0; k < n; ++k){
                dum = matrix[imax][k];
                matrix[imax][k] =  matrix[j][k];
                matrix[j][k] = dum;
            }
            vv[imax] = vv[j];
        }

        index[j] = imax;

        if(matrix[j][j] == 0.0){
            matrix[j][j] = TINY;
        }

        if(j != n-1){
            dum = 1.0/matrix[j][j];
            for(i = j+1; i < n; ++i){
                matrix[i][j] *= dum;
            }
        }
    }
    free(vv);
}

void LUSolving(float** matrix, int n, int* index, float* b){
    int i, ii = 0, ip, j;
    float sum;

    for(i = 0; i < n; ++i){
        ip = index[i];
        sum = b[ip];
        b[ip] = b[i];

        if(ii){
            for(j = ii; j <= i-1; ++j){
                sum -= matrix[i][j] *b[j];
            }
        }
        else if(sum){
            ii = i;
        }
        b[i] = sum;
    }

    for(i = n-1; i >= 0; --i){
        sum = b[i];

        for(j = i + 1; j < n; ++j){
            sum -= matrix[i][j] * b[j];
        }

        b[i] = sum/matrix[i][i];
    }
}

//assumes params is allocated and initialised with 0
void getGCoefficients(float* params, Vertex q, Vertex normal, float h, int N, Vertex** verts, float(*radWeight)(float, float), Vertex** projections){
    float** coeffMatrix = new float*[10];
    float* coords = new float[2];
    float minVal = INT_MAX;
    float factor = 1;

    
    for(int i = 0; i < 10; ++i){
        coeffMatrix[i] = new float[10]();// initialize the entries
        for(int j = 0; j < 10; ++j){
            for(int it = 0; it < N; ++it){
                getLocalCoords(coords, *projections[it], q, normal);
                coeffMatrix[i][j] += funcCoeff(i,coords[0], coords[1]) * funcCoeff(j,coords[0], coords[1]) * 2 * radWeight((*verts[it]-q).dist(),h);
            }
            if(fabs(coeffMatrix[i][j]) < minVal){
                minVal = fabs(coeffMatrix[i][j]);
            }
        }
        for(int it = 0; it < N; ++it){
            getLocalCoords(coords, *projections[it], q, normal);
            params[i] += 2 * radWeight((*verts[it]-q).dist(),h) * funcCoeff(i, coords[0], coords[1]) * (normal * (*verts[it]-q));
        }
        
    }

    while(minVal < 1.0){
        factor *= 10.0;
        minVal *= 10.0;
    }


    
    int *index = new int[10];
    
    std::cout << "\nCoeff matrix before\n";
    for(int i = 0; i < 10; ++i, std::cout << std::endl){
        for(int j = 0; j < 10; ++j){
            std::cout << coeffMatrix[i][j] << ' ';
        }
    }
    std::cout << "Before comp\n";
    for(int k = 0; k < 10; ++k){
            std::cout << params[k] << std::endl;
        }

    std::cout << "_____________________";
    LUDecomposition(coeffMatrix, 10, index);

    std::cout << "\nCoeff matrix after\n";
    for(int i = 0; i < 10; ++i, std::cout << std::endl){
        for(int j = 0; j < 10; ++j){
            std::cout << coeffMatrix[i][j] << ' ';
        }
    }

    LUSolving(coeffMatrix, 10, index, params);

    for(int k = 0; k < 10; ++k){
        params[k] = params[k]/factor;
        }
}



void getGCoefficients(float* params, Vertex q, Vertex normal, float h, int N, std::vector<Vertex*> verts, float(*radWeight)(float, float)){
    float** coeffMatrix = new float*[10];
    float* coords = new float[2];

    for(int i = 0; i < 10; ++i){
        params[i] = 0;
    }

    for(int i = 0; i < 10; ++i){
        coeffMatrix[i] = new float[10]();// initialize the entries
        for(int j = 0; j < 10; ++j){
            for(int it = 0; it < N; ++it){
                // std::cout << "proj:" << *verts[it]->projection << std::endl;// << ", q:"<< q << ", normal:" << normal <<std::endl;
                getLocalCoords(coords, *verts[it]->projection, q, normal);
                coeffMatrix[i][j] += funcCoeff(i,coords[0], coords[1]) * funcCoeff(j,coords[0], coords[1]) * 2 * radWeight((*verts[it]-q).dist(),h);

            }

        }
        for(int it = 0; it < N; ++it){
            getLocalCoords(coords, *verts[it]->projection, q, normal);
            params[i] += 2 * radWeight((*verts[it]-q).dist(),h) * funcCoeff(i, coords[0], coords[1]) * (normal * (*verts[it]-q));
            // std::cout << "params: " << i  << ", value: " << params[i] << std::endl;
        }
        
    }


    GaussJ(coeffMatrix, 10, params);

    // checking for nan's
    for(int k = 0; k < 10; ++k){
            if(params[k] != params[k]){
                params[k] = 0;
            }
        }
}

double solveCubic(double a, double b, double c){
    double t, q = (a*a - 3 * b)/9;
    double r = (2*a*a*a - 9 * a * b + 27 * c)/54;
    double* x = new double[3];

    // std::cout << "a: " << a << " b: " << b << " c: " << c << std::endl;

    // std::cout << "r: " << r << " q: " << q << std::endl;
    
    double r2 = r*r;
    double q3 = q*q*q;

    if(r2 <= q3){
        // 3 real roots
        t = acos(r/sqrt(q*q*q));
        x[0] = -2 * sqrt(q) * cos(t/3) - a/3;
        x[1] = -2 * sqrt(q) * cos((t + 2 * PI)/3) - a/3;
        x[2] = -2 * sqrt(q) * cos((t - 2 * PI)/3) - a/3;
    }
    else{
        // one real root, complex ones are not useful
        double B, A = -SGN(r) * pow(fabs(r) + sqrt(r * r - q * q * q), 1/3);
        if(A)
            B = q/A;
        else B = 0;

        return (A+B) - a/3;
    }

    return mini(x[0], mini(x[1], x[2]));
}

void computeNormals(Vertex** verts, int N, Vertex* r, Vertex *normal, float(*radWeight)(float,float), float h){
    float** cM = new float*[3];

    for(int i = 0; i < 3; ++i){
        cM[i] = new float[3]();
        for(int j = 0; j < 3; ++j){
            for(int it = 0; it < N; ++it){
                cM[i][j] += radWeight((*verts[it] - *r).dist(), h) * ((*verts[it])[i] - (*r)[i]) * ((*verts[it])[j] - (*r)[j]);
            }
        }
    }

    float a,b,c;
    
    a = -cM[2][2] - cM[0][0] - cM[1][1];

    b = cM[0][0] * cM[1][1] + cM[0][0] * cM[2][2] + cM[2][2] * cM[1][1]  
        - cM[0][2] * cM[2][0] - cM[2][1] * cM[1][2] - cM[1][0] * cM[0][1];
    
    c = cM[0][1] * cM[1][0] * cM[2][2] 
        + cM[0][0] * cM[1][2] * cM[2][1] 
        + cM[0][2] * cM[2][0] * cM[1][1] 
        - cM[0][0] * cM[1][1] * cM[2][2] 
        - cM[1][0] * cM[2][1] * cM[0][2] 
        - cM[0][1] * cM[1][2] * cM[2][0];

    float eigenvalue = solveCubic(a,b,c);
    float* res = new float[3]();

    
    cM[0][0] -= eigenvalue;
    cM[1][1] -= eigenvalue;
    cM[2][2] -= eigenvalue;


    res[0] = 1;


    if(fabs(cM[0][2]) > TINY && fabs(cM[1][2]/cM[0][2]*cM[0][1] - cM[1][1]) > TINY){
        res[1] = (cM[1][0] - cM[1][2]/cM[0][2]*cM[0][0])/(cM[1][2]/cM[0][2]*cM[0][1] - cM[1][1]);
        res[2] = (-cM[0][0] - cM[0][1] * res[1])/cM[0][2];
    }
    else if(fabs(cM[1][2]) > TINY && fabs(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]) > TINY){
        res[1] = (cM[2][0] - cM[2][2]/cM[1][2]*cM[1][0])/(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]);
        res[2] = (-cM[1][0] - cM[1][1] * res[1])/cM[1][2];
    }
    else if(fabs(cM[2][2]) > TINY && fabs(cM[0][2]/cM[2][2]*cM[2][1] - cM[0][1]) > TINY){
        res[1] = (cM[2][0] - cM[2][2]/cM[1][2]*cM[1][0])/(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]);
        res[2] = (-cM[1][0] - cM[1][1] * res[1])/cM[1][2];
    }
    else {
        res[2] = 1;
        res[1] = -2;
    }


    normal->x = res[0];
    normal->y = res[1];
    normal->z = res[2];

    normal->normalize();
}


void computeNormals(std::vector<Vertex*> verts, int N, Vertex* r, Vertex *normal, float(*radWeight)(float,float), float h){
    double** cM = new double*[3];
    double minVal = 1000;
    int factor = 1;
    for(int i = 0; i < 3; ++i){
        cM[i] = new double[3]();
        for(int j = 0; j < 3; ++j){
            for(int it = 0; it < N; ++it){
                cM[i][j] += radWeight((*verts[it] - *r).dist(), h) * ((*verts[it])[i] - (*r)[i]) * ((*verts[it])[j] - (*r)[j]);
            }
            if(fabs(cM[i][j]) < minVal && cM[i][j]){
                minVal = fabs(cM[i][j]);
            }
        }
    }

    while(minVal < 1.0){
        factor *= 10.0;
        minVal *= 10.0;
    }

    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            cM[i][j] *= factor;
        }
    }
    double a,b,c;
    
    a = -cM[2][2] - cM[0][0] - cM[1][1];

    b = cM[0][0] * cM[1][1] + cM[0][0] * cM[2][2] + cM[2][2] * cM[1][1]  
        - cM[0][2] * cM[2][0] - cM[2][1] * cM[1][2] - cM[1][0] * cM[0][1];
    
    c = cM[0][1] * cM[1][0] * cM[2][2] 
        + cM[0][0] * cM[1][2] * cM[2][1] 
        + cM[0][2] * cM[2][0] * cM[1][1] 
        - cM[0][0] * cM[1][1] * cM[2][2] 
        - cM[1][0] * cM[2][1] * cM[0][2] 
        - cM[0][1] * cM[1][2] * cM[2][0];

    double eigenvalue = solveCubic(a,b,c) / factor;
    double* res = new double[3]();

    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            cM[i][j] /= factor;
        }
    }
    
    cM[0][0] -= eigenvalue;
    cM[1][1] -= eigenvalue;
    cM[2][2] -= eigenvalue;


    res[0] = 1;


    if(fabs(cM[0][2]) > TINY && fabs(cM[1][2]/cM[0][2]*cM[0][1] - cM[1][1]) > TINY){
        res[1] = (cM[1][0] - cM[1][2]/cM[0][2]*cM[0][0])/(cM[1][2]/cM[0][2]*cM[0][1] - cM[1][1]);
        res[2] = (-cM[0][0] - cM[0][1] * res[1])/cM[0][2];
    }
    else if(fabs(cM[1][2]) > TINY && fabs(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]) > TINY){
        res[1] = (cM[2][0] - cM[2][2]/cM[1][2]*cM[1][0])/(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]);
        res[2] = (-cM[1][0] - cM[1][1] * res[1])/cM[1][2];
    }
    else if(fabs(cM[2][2]) > TINY && fabs(cM[0][2]/cM[2][2]*cM[2][1] - cM[0][1]) > TINY){
        res[1] = (cM[2][0] - cM[2][2]/cM[1][2]*cM[1][0])/(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]);
        res[2] = (-cM[1][0] - cM[1][1] * res[1])/cM[1][2];
    }
    else {
        res[2] = 1;
        res[1] = -2;
    }


    normal->x = res[0];
    normal->y = res[1];
    normal->z = res[2];

    if(normal->x != normal->x || normal->y != normal->y || normal->z != normal->z){
        normal->x = normal->y = normal->z = 1;
    }

    *r->normal = normal->normalize();
}


float qFuncCG(std::vector<Vertex*> verts, int N, Vertex q, Vertex normal, float(*radWeight)(float,float), float h){
    float res = 0.0;
    
    for(int i = 0; i < N; ++i){
        res += (normal * (*verts[i] - q))*(normal * (*verts[i] - q)) * radWeight((*verts[i]-q).dist(),h);
    }
    return res;
}

void qDerivCG(std::vector<Vertex*> verts, int N, Vertex q, Vertex *res, Vertex normal, float(*radWeight)(float,float), float h){
    res->x = res->y = res->z = 0.0;
    
    for(int i = 0; i < N; ++i){
        res->x += 2.0 * (normal * (*verts[i] - q)) * (-normal.x) * radWeight((*verts[i]-q).dist(),h)  + (normal * (*verts[i] - q))*(normal * (*verts[i] - q)) * radWeight((*verts[i]-q).dist(),h) * 2 * (verts[i]->x - q.x) / h / h;


        res->y += 2.0 * (normal * (*verts[i] - q)) * (-normal.y) * radWeight((*verts[i]-q).dist(),h)  + (normal * (*verts[i] - q))*(normal * (*verts[i] - q)) * radWeight((*verts[i]-q).dist(),h) * 2 * (verts[i]->y - q.y) / h / h;

        res->z += 2.0 * (normal * (*verts[i] - q)) * (-normal.z) * radWeight((*verts[i]-q).dist(),h)  + (normal * (*verts[i] - q))*(normal * (*verts[i] - q)) * radWeight((*verts[i]-q).dist(),h) * 2 * (verts[i]->z - q.z) / h / h;
    }
}

void projectSurface(float h, int N, Vertex** verts, float(*radWeight)(float, float), float epsilon, float (*polynomial)(float*, float, float), Grid* grid){
    float oldT;
    Vertex oldQ;
    float* params3 = new float[10];
    std::vector<Vertex*> neighbours;

    for(int i = 0; i < 10; ++i){
        params3[i] = 1;
    }

    // compute normals

    for(int i = 0; i < N; ++i){
        verts[i]->t = 0;
        getClosestCells(grid, verts[i], neighbours, true);
        computeNormals(neighbours, neighbours.size(), verts[i], verts[i]->normal, radWeight, h);
        *verts[i]-> projection = *verts[i] + *verts[i]->normal * verts[i]->t;
        neighbours.clear();
    }



    #ifndef DEBUG_NORMALS

    for(int i = 0; i < N; ++i){
        #ifdef DEFAULT_NBR
        do{
            // save old values of t and q
            oldT = verts[i]->t;
            oldQ = *verts[i] +  *verts[i]->normal * verts[i]->t ;
            //compute new value for t
            dbrent(-h/2, verts[i]->t, h/2, TOL, &verts[i]->t, neighbours.size(), neighbours, radWeight, *verts[i]->normal, *verts[i], h);

            // update the q value depending on the newly found t
            *verts[i]->projection = *verts[i] + *verts[i]->normal * verts[i]->t;
            // apply conjugate gradients to minimise q
            
            // conjGradient(verts[i]->projection, TOL, fret, qFuncCG, qDerivCG);
            
            // update the new t and normal from the q minimization

            verts[i]->t = (*verts[i]->projection - *verts[i]).dist();

            if(verts[i]->t){
                *verts[i]->normal = (*verts[i]->projection - *verts[i]) * (1.0/verts[i]->t);
            }
            else{
                *verts[i]->normal = *verts[i]->projection - *verts[i];
            }
            

        } while (fabs(oldT - verts[i]->t) > epsilon || (oldQ - *verts[i]->projection).dist() > epsilon);
        // get coefficients for g
       
        
        verts[i]->params = new float[10];
        getGCoefficients(verts[i]->params, *verts[i]->projection, *verts[i]->normal, h, neighbours.size(), neighbours, radWeight);

        #endif
            

        
        // new OCTREE version
        #ifndef OCTREE_NBR        

        getClosestCells(grid, verts[i], neighbours, true);


       
        
        do{
            // save old values of t and q
            oldT = verts[i]->t;
            oldQ = *verts[i] +  *verts[i]->normal * verts[i]->t ;
            //compute new value for t
            
            dbrent(-h/2, verts[i]->t, h/2, TOL, &verts[i]->t, neighbours.size(), neighbours, radWeight, *verts[i]->normal, *verts[i], h);
            
            // update the q value depending on the newly found t
            
            *verts[i]->projection = *verts[i] + *verts[i]->normal * verts[i]->t;
            
            // apply conjugate gradients to minimise q
            // conjGradient(verts[i]->projection, TOL, fret, neighbours, neighbours.size(), *verts[i]->normal, radWeight, h);
            
            // update the new t and normal from the q minimization
           
            verts[i]->t = (*verts[i]->projection - *verts[i]).dist();

            if(verts[i]->t){
                *verts[i]->normal = (*verts[i]->projection - *verts[i]) * (1.0/verts[i]->t);
            }
            else{
                *verts[i]->normal = *verts[i]->projection - *verts[i];
            }
           
        } while (fabs(oldT - verts[i]->t) > epsilon || (oldQ - *verts[i]->projection).dist() > epsilon);
        
        // get coefficients for g
        getGCoefficients(verts[i]->params, *verts[i]->projection, *verts[i]->normal, h, neighbours.size(), neighbours, radWeight);
       
        
        *verts[i]->projection = *verts[i] + *verts[i]->normal * (verts[i]->t + polynomial(verts[i]->params, 0, 0));
       
        neighbours.clear(); 
        #endif
        
        
        #ifdef PRINT_DEBUG
        std::cout << std::fixed << verts[i]->x << ' ' << verts[i]->y << ' ' << verts[i]->z << std::endl;
        std::cout << std::fixed << verts[i]->normal->x << ' ' << verts[i]->normal->y << ' ' << verts[i]->normal->z << std::endl;
        std::cout << std::fixed << verts[i]->projection->x << ' ' << verts[i]->projection->y << ' ' << verts[i]->projection->z << std::endl; 
        std::cout << std::fixed << verts[i]->t << std::endl;
        for(int j = 0; j < 10; ++j){
            std::cout << std::fixed << verts[i]->params[j] << ' ';
        }
        std::cout << std::endl;
        #endif
    }

    #endif
   
}

inline float mini(float a, float b){
    return (fabs(a)<fabs(b) ? a : b);
}

void getClosestCells(Grid* grid, Vertex* vert, std::vector<Vertex*>& vertices, bool shouldIncludeItself){
    int x,y,z;

    x = (vert->x - grid->xMin)/grid->neglDist;
    y = (vert->y - grid->yMin)/grid->neglDist;
    z = (vert->z - grid->zMin)/grid->neglDist;

    Vertex corner, minCorner, initCorner;
    int cornerIndex;
    int a,b,c;
    float mini = 100;

    initCorner = Vertex(x*grid->neglDist, y*grid->neglDist, z*grid->neglDist);

    // std::cout << "corner" << initCorner << std::endl;

    for(int i =  0; i < 8; ++i){
        corner = initCorner;
        a = i%2;
        b = i/2%2;
        c = i/4%2;
        corner.x += a*grid->neglDist;
        corner.y += b*grid->neglDist;
        corner.z += c*grid->neglDist;
        // std::cout << "new corner: " << corner << std::endl;
        // std::cout << "distance: " << corner.dist(*vert) << std::endl;
        if(corner.dist(*vert) < mini){
            minCorner = corner;
            cornerIndex = i;
            mini = corner.dist(*vert);
        }
    }

    // std::cout << "corner index: " << cornerIndex << std::endl;

    // std::cout << "Vertex at coords: " << x << ' ' << y << ' ' << z << std::endl;
        // std::cout << "neighbour cell of :" << x << y << z << " at vertex: " << *vert << std::endl; 

    for(int i = 0; i < 8; ++i){
        // std::cout << currX << ' ' << currY << ' ' <<  currZ << std::endl;
        // std::cout << x + nbrCells[cornerIndex][i][0] << ' ' << y + nbrCells[cornerIndex][i][1] << ' ' << z + nbrCells[cornerIndex][i][2] << std::endl;
        Octree* currNbr = (*grid)(x + nbrCells[cornerIndex][i][0],y + nbrCells[cornerIndex][i][1],z + nbrCells[cornerIndex][i][2]);
        getVertices(currNbr, vertices, vert);
        // std::cout << "_____________________" << std::endl;
    }

    if(shouldIncludeItself){
        vertices.emplace_back(vert);
    }
}

const int nbrCells[8][8][3] = {
    {{0,0,0}, {-1,0,0}, {0,0,-1}, {-1,0,-1}, {0,-1,0}, {-1,-1,0}, {0,-1,-1}, {-1,-1,-1}}, //index 0 - v
    {{0,0,0}, {1,0,0},  {0,0,-1}, {1,0,-1},  {0,-1,0}, {1,-1,0},  {0,-1,-1}, {1,-1,-1}}, //index 1 - v
    {{0,0,0}, {-1,0,0},  {0,0,-1}, {-1,0,-1},  {0,1,0},   {-1,1,0},  {0,1,-1},   {-1,1,-1}}, //index 2 - v
    {{0,0,0}, {1,0,0},  {0,0,-1}, {1,0,-1},  {0,1,0},   {1,1,0},  {0,1,-1},   {1,1,-1}}, //index 3 - v 

    {{0,0,0}, {-1,0,0},  {0,0,1}, {-1,0,1}, {0,-1,0}, {-1,-1,0}, {0,-1,1},  {-1,-1,1}}, //index 4 - v
    {{0,0,0}, {1,0,0},   {0,0,1}, {1,0,1},  {0,-1,0}, {1,-1,0},  {0,-1,1},  {1,-1,1}}, //index 5 - v
    {{0,0,0}, {-1,0,0},  {0,0,1}, {-1,0,1},  {0,1,0},  {-1,1,0},   {0,1,1},   {-1,1,1}}, //index 6
    {{0,0,0}, {1,0,0},  {0,0,1}, {1,0,1}, {0,1,0},  {1,1,0},  {0,1,1},   {1,1,1}}, //index 7 - v
};

void getVertices(Octree* oct, std::vector<Vertex*>& vertices, Vertex* vert){
    // std::cout << oct->hasPoint << std::endl;
    if(oct->isRetVal)
        return;
    if(!oct->hasPoint)
        return;
    if(oct->point != nullptr){
        // std::cout << "Has point:" << *oct->point << std::endl;
        // std::cout << "Centroid: " << *oct->centroid << " No points:" << oct->pointCount << std::endl;
        if(oct->point != vert){
            vertices.emplace_back(oct->point);
        }
        return;
    }
    for(int i = 0; i < 8; ++i){
        // std::cout << "Octree: " << i << std::endl;
        // std::cout << "Centroid: " << *oct->centroid << " No points:" << oct->pointCount << std::endl;
        getVertices(oct->children[i], vertices, vert);
    }
}

struct ComparePriority { 
    bool operator()(Vertex* const v1, Vertex* const v2) 
    { 
        return v1->priority > v2->priority; 
    } 
};

void downSampler(Vertex** verts, int N, int percentage, Grid* grid){
    std::vector<Vertex*> neighbours, currentNbr;
    std::multimap<float, Vertex*> downSamplePQ;

    for(int i = 0; i < N; ++i){
        getClosestCells(grid, verts[i], neighbours, false);
        float dist = 0.0;
        for(size_t j = 0; j < neighbours.size(); ++j){
            dist += verts[i]->dist(*neighbours[j]->projection);
        }
        dist/=neighbours.size();
        
        
        verts[i]->priority = dist;
        
        // std::cout << "Priority: " << verts[i]->priority << " T: " << verts[i]->t << std::endl;

        downSamplePQ.insert(std::pair<float, Vertex*>(verts[i]->priority, verts[i]));
        neighbours.clear();
    }
    size_t newVal = N - (float)percentage/100.0*N;

    while(downSamplePQ.size() > newVal){
        Vertex* top = downSamplePQ.begin()->second;
        top->shouldDraw = (float)(N - downSamplePQ.size())*100.0/N;

        getClosestCells(grid, top, currentNbr, false);

        for(size_t i = 0; i < currentNbr.size(); ++i){
            getClosestCells(grid, currentNbr[i], neighbours, false);
            
            float dist = 0.0;
            for(size_t j = 0; j < neighbours.size(); ++j){
                if(!(neighbours[j] == top))
                    dist += currentNbr[i]->dist(*neighbours[j]->projection);
            }

            if(neighbours.size()>1)
                dist/=(neighbours.size()-1);

        
            currentNbr[i]->priority = dist;

            std::pair<std::multimap<float, Vertex*>::iterator, std::multimap<float, Vertex*>::iterator> results = downSamplePQ.equal_range(currentNbr[i]->priority);
            

            for(auto it=results.first; it!=results.second; ++it){
                if(it->second == currentNbr[i]){ //found current neighbour; need to erase it and add it again
                    downSamplePQ.erase(it);
                    downSamplePQ.insert(std::pair<float, Vertex*>(currentNbr[i]->priority, currentNbr[i]));
                    break;
                }
            }
        }

        neighbours.clear();
        currentNbr.clear();
       
        downSamplePQ.erase(downSamplePQ.begin());
    }
}

void testEigen(double ** cM){
    std::cout << std::endl;
    std::cout << cM[0][0] << ' ' << cM[0][1] << ' ' << cM[0][2] << std::endl;
    std::cout << cM[1][0] << ' ' << cM[1][1] << ' ' << cM[1][2] << std::endl;
    std::cout << cM[2][0] << ' ' << cM[2][1] << ' ' << cM[2][2] << std::endl;
    std::cout << std::endl;
    double a,b,c;
    double minVal = 1000;
    int factor = 1;

    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            if(fabs(cM[i][j]) < minVal){
                    minVal = fabs(cM[i][j]);
            }
        }
    }

    std::cout << "min val:" << minVal << std::endl;

    while(minVal < 1.0){
        factor *= 10.0;
        minVal *= 10.0;
    }

    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            cM[i][j] *= factor;
        }
    }
    
    a = -cM[2][2] - cM[0][0] - cM[1][1];

    b = cM[0][0] * cM[1][1] + cM[0][0] * cM[2][2] + cM[2][2] * cM[1][1]  
        - cM[0][2] * cM[2][0] - cM[2][1] * cM[1][2] - cM[1][0] * cM[0][1];
    
    c = cM[0][1] * cM[1][0] * cM[2][2] 
        + cM[0][0] * cM[1][2] * cM[2][1] 
        + cM[0][2] * cM[2][0] * cM[1][1] 
        - cM[0][0] * cM[1][1] * cM[2][2] 
        - cM[1][0] * cM[2][1] * cM[0][2] 
        - cM[0][1] * cM[1][2] * cM[2][0];

    double eigenvalue = solveCubic(a,b,c) / factor;
    double* res = new double[3]();

    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            cM[i][j] /= factor;
        }
    }

    
    cM[0][0] -= eigenvalue;
    cM[1][1] -= eigenvalue;
    cM[2][2] -= eigenvalue;


    res[0] = 1;


    if(fabs(cM[0][2]) > TINY && fabs(cM[1][2]/cM[0][2]*cM[0][1] - cM[1][1]) > TINY){
        res[1] = (cM[1][0] - cM[1][2]/cM[0][2]*cM[0][0])/(cM[1][2]/cM[0][2]*cM[0][1] - cM[1][1]);
        res[2] = (-cM[0][0] - cM[0][1] * res[1])/cM[0][2];
    }
    else if(fabs(cM[1][2]) > TINY && fabs(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]) > TINY){
        res[1] = (cM[2][0] - cM[2][2]/cM[1][2]*cM[1][0])/(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]);
        res[2] = (-cM[1][0] - cM[1][1] * res[1])/cM[1][2];
    }
    else if(fabs(cM[2][2]) > TINY && fabs(cM[0][2]/cM[2][2]*cM[2][1] - cM[0][1]) > TINY){
        res[1] = (cM[2][0] - cM[2][2]/cM[1][2]*cM[1][0])/(cM[2][2]/cM[1][2]*cM[1][1] - cM[2][1]);
        res[2] = (-cM[1][0] - cM[1][1] * res[1])/cM[1][2];
    }
    else {
        res[2] = 1;
        res[1] = -2;
    }


    Vertex* normal = new Vertex();

    normal->x = res[0];
    normal->y = res[1];
    normal->z = res[2];

    if(normal->x != normal->x || normal->y != normal->y || normal->z != normal->z){
        normal->x = normal->y = normal->z = 1;
    }

    
    std::cout <<"Normal: " << *normal << std::endl;
    *normal = normal->normalize();
    std::cout <<"Normalised normal: " << *normal << std::endl;
}

void testOctree(Vertex** verts, int N, Grid* grid){
    std::vector<Vertex*> nbr;
    for(int i = 0; i < N; ++i){
        getClosestCells(grid, verts[i], nbr, true);
        std::cout << "Neighbours of " << *verts[i] << ' ' << nbr.size() << std::endl;
        nbr.clear();
    }
}

void getCclockwise(float** points, int& p1, int& p2, int& p3, int i, int j, int k){
    float det;
    p1 = i;
    p2 = j;
    p3 = k;
    
    det = points[p1][0] * points[p2][1] + points[p2][0] * points[p3][1] + points[p1][1] * points[p3][0] - points[p3][0] * points[p2][1] - points[p1][0] * points[p3][1] - points[p2][0] * points[p1][1];

    if(det > 0){
        return;
    }
    

    p1 = i;
    p2 = k;
    p3 = j;
    
    det = points[p1][0] * points[p2][1] + points[p2][0] * points[p3][1] + points[p1][1] * points[p3][0] - points[p3][0] * points[p2][1] - points[p1][0] * points[p3][1] - points[p2][0] * points[p1][1];

    if(det > 0){
        return;
    }

    p1 = j;
    p2 = i;
    p3 = k;
    
    det = points[p1][0] * points[p2][1] + points[p2][0] * points[p3][1] + points[p1][1] * points[p3][0] - points[p3][0] * points[p2][1] - points[p1][0] * points[p3][1] - points[p2][0] * points[p1][1];

    if(det > 0){
        return;
    }

    p1 = j;
    p2 = k;
    p3 = i;
    
    det = points[p1][0] * points[p2][1] + points[p2][0] * points[p3][1] + points[p1][1] * points[p3][0] - points[p3][0] * points[p2][1] - points[p1][0] * points[p3][1] - points[p2][0] * points[p1][1];

    if(det > 0){
        return;
    }

    p1 = k;
    p2 = i;
    p3 = j;
    
    det = points[p1][0] * points[p2][1] + points[p2][0] * points[p3][1] + points[p1][1] * points[p3][0] - points[p3][0] * points[p2][1] - points[p1][0] * points[p3][1] - points[p2][0] * points[p1][1];

    if(det > 0){
        return;
    }

    p1 = k;
    p2 = j;
    p3 = i;
    
    det = points[p1][0] * points[p2][1] + points[p2][0] * points[p3][1] + points[p1][1] * points[p3][0] - points[p3][0] * points[p2][1] - points[p1][0] * points[p3][1] - points[p2][0] * points[p1][1];

    if(det > 0){
        return;
    }
}

void getCircumcentre(float** points, int p1, int p2, int p3, float* result){
    // gets the circumcentre of a triangle of points p1,p2,p3
    float x1,y1,x2,y2,x3,y3;
    
    x1 = points[p1][0];
    y1 = points[p1][1];
    x2 = points[p2][0];
    y2 = points[p2][1];
    x3 = points[p3][0];
    y3 = points[p3][1];

    // std::cout << "current triangle" << std::endl;
    // std::cout << x1 << ' ' << y1 << std::endl;
    // std::cout << x2 << ' ' << y2 << std::endl;
    // std::cout << x3 << ' ' << y3 << std::endl;

    result[0] = ((x1*x1 + y1*y1)*(y2-y3) - y1*(x2*x2 + y2*y2 - x3*x3 - y3*y3) - y2*(x3*x3 + y3*y3) + y3*(x2*x2 + y2*y2))/(2*(x1*(y2-y3) - y1*(x2-x3) + x2*y3 - x3*y2));

    result[1] = (x1*(x2*x2 + y2*y2 - x3*x3 - y3*y3) + x2*(x3*x3 + y3*y3) - x3*(x2*x2 + y2*y2) -(x1*x1 + y1*y1)*(x2-x3))/(2*(x1*(y2-y3) - y1*(x2-x3) + x2*y3 - x3*y2));

    // std::cout << "circumcentre: " << result[0] << ' ' << result[1] << std::endl;
}

bool isEmpty(float** points, int n, int p1, int p2, int p3){
    // goes through all points besides p1, p2, p3 and checks for inclusion
    float ax,ay,az,bx,by,bz,cx,cy,cz,dx,dy,dz, det, *result, maxi = 0.0;
    result = new float[2];
    ax = points[p1][0];
    ay = points[p1][1];
    az = ax*ax + ay*ay;

    bx = points[p2][0];
    by = points[p2][1];
    bz = bx*bx + by*by;

    cx = points[p3][0];
    cy = points[p3][1];
    cz = cx*cx + cy*cy;
    for(int i = 0; i < n; ++i){
        if(i != p1 && i!= p2 && i!= p3){
            dx = points[i][0];
            dy = points[i][1];
            dz = dx*dx + dy*dy;

            det = (ax-dx) * (by-dy) * ((cx-dx)*(cx-dx) + (cy-dy)*(cy-dy)) 
            + (bx-dx) * (cy - dy) * ((ax-dx) * (ax-dx) + (ay-dy) *(ay-dy)) 
            + (ay-dy) * (cx-dx) * ((bx-dx) * (bx-dx) + (by-dy)*(by-dy)) 
            - ((ax-dx) * (ax-dx) + (ay-dy) *(ay-dy)) * (by-dy) * (cx-dx)
            - ((bx-dx) * (bx-dx) + (by-dy)*(by-dy)) * (cy - dy) * (ax-dx)
            - ((cx-dx)*(cx-dx) + (cy-dy)*(cy-dy)) * (bx-dx) * (ay-dy); 
            if(det > 0)
                return false;
            getCircumcentre(points,p1,p2,p3,result);
            if((points[i][0]-result[0])*(points[i][0]-result[0]) + (points[i][1]-result[1])* (points[i][1]-result[1]) > maxi)
                maxi = (points[i][0]-result[0])*(points[i][0]-result[0]) + (points[i][1]-result[1])* (points[i][1]-result[1]);
        }
    }
    if(maxi < MAXVOROSIZE)
        return true;
    return false;
}

void getVoronoiVertex(float** points, int n, float* result){
    float largestRadius = 0.0, currRadius, *minim, *maxim;
    //00 left bottom corner x -min
    //01 left bottom corner y -min
    //02 right top corner x -max
    //03 right top corner y -max


    minim = new float[2];
    maxim = new float[2];

    minim[0] = minim[1] = INT_MAX;
    maxim[0] = maxim[1] = INT_MIN;


    for(int i = 0; i < n; ++i){
        if(points[i][0] < minim[0]){
            minim[0] = points[i][0];
        }
        if(points[i][0] > maxim[0]){
            maxim[0] = points[i][0];
        }
        if(points[i][1] < minim[1]){
            minim[1] = points[i][1];
        }
        if(points[i][1] > maxim[1]){
            maxim[1] = points[i][1];
        }
    }

    minim[0]-=TINY;
    minim[1]-=TINY;
    maxim[0]+=TINY;
    maxim[1]+=TINY;

    for(int i = 0; i < n; ++i){
        for(int j = i; j < n; ++j){
            for(int k = j; k < n; ++k){
                int p1, p2, p3;

                getCclockwise(points, p1, p2, p3, i, j, k);

                // if nothing inside, save the result as max circumference

                if(isEmpty(points, n, p1, p2, p3)){
                    float a,b,c;
                    a = sqrt((points[p1][0] - points[p2][0]) * (points[p1][0] - points[p2][0]) + (points[p1][1] - points[p2][1]) * (points[p1][1] - points[p2][1]));
                    b = sqrt((points[p1][0] - points[p3][0]) * (points[p1][0] - points[p3][0]) + (points[p1][1] - points[p3][1]) * (points[p1][1] - points[p3][1]));
                    c = sqrt((points[p2][0] - points[p3][0]) * (points[p2][0] - points[p3][0]) + (points[p2][1] - points[p3][1]) * (points[p2][1] - points[p3][1]));  
                    
                    // sif co-linear
                    if((a+b+c) * (-a+b+c) * (a-b+c) * (a+b-c) < TINY || (fabs(a+b-c)<=0.00001) || (fabs(a+c-b)<=0.00001) || (fabs(b+c-a) <=0.00001))
                        continue;
                    currRadius = (a*b*c)/(sqrt((a+b+c) * (-a+b+c) * (a-b+c) * (a+b-c)));

                    if(currRadius > largestRadius){
                        getCircumcentre(points, p1, p2, p3, result);
                        // if inside the imaginary rectangle
                        if(result[0] > minim[0] && result[0] < maxim[0] && result[1] > minim[1] && result[1] < maxim[1]){
                            largestRadius = currRadius;
                        }
                        else{
                            result[0] = result[1] = 0;
                        }
                        
                    }
                }
            }
        }
    }
}


void upSampler(Vertex** verts, std::vector<Vertex*>& vertices, int N, int percentage, Grid* grid){
    std::default_random_engine gen(std::random_device{}());
    std::uniform_int_distribution<>distr(0,N-1);

    int newVal = (float)percentage/100.0*N;
    std::vector<Vertex*> neighbours;
    float* params = new float[10]{};

    for(int i = 0; i < N; ++i){
        Vertex *point = new Vertex(), *currVert;
        currVert = verts[i];

        getClosestCells(grid, currVert, neighbours, true);
        
        float** nbrPoints = new float*[neighbours.size() + 3]; 



        for(size_t j = 0; j < neighbours.size(); ++j){
            nbrPoints[j] = new float[2];
            getLocalCoords(nbrPoints[j], *neighbours[j], *currVert + *currVert->normal * currVert->t, *currVert->normal);
        }
        nbrPoints[neighbours.size()] = new float[2];

        //first vertex

        getVoronoiVertex(nbrPoints, neighbours.size(), nbrPoints[neighbours.size()]);

        getWorldCoords(nbrPoints[neighbours.size()], point, *currVert + *currVert->normal * currVert->t, *currVert->normal, params);
        point->normal = currVert->normal;
        point->upLevel = (float)(vertices.size()/(newVal / percentage));
        if(!(*point == *currVert)){
            vertices.push_back(point);
        }

        //second vertex

        point = new Vertex();

        nbrPoints[neighbours.size()+1] = new float[2];

        getVoronoiVertex(nbrPoints, neighbours.size()+1, nbrPoints[neighbours.size()+1]);

        getWorldCoords(nbrPoints[neighbours.size()+1], point, *currVert + *currVert->normal * currVert->t, *currVert->normal, params);
        point->normal = currVert->normal;
        point->upLevel = (float)(vertices.size()/(newVal / percentage));

        if(!(*point == *currVert)){
            vertices.push_back(point);
        }

        // third vertex

        nbrPoints[neighbours.size()+2] = new float[2];

        point = new Vertex();
        getVoronoiVertex(nbrPoints, neighbours.size()+2, nbrPoints[neighbours.size()+2]);

        getWorldCoords(nbrPoints[neighbours.size() + 2], point, *currVert + *currVert->normal * currVert->t, *currVert->normal, params);
        point->normal = currVert->normal;
        point->upLevel = (float)(vertices.size()/(newVal / percentage));

        if(!(*point == *currVert)){
            vertices.push_back(point);
        }

        neighbours.clear();
    }
}

void testVoronoi(){
    float** points = new float*[4];

    points[0] = new float[2];
    points[1] = new float[2];
    points[2] = new float[2];
    points[3] = new float[2];
    float* result = new float[2];

    points[0][0] = -3;
    points[0][1] = 2;
    points[1][0] = 1;
    points[1][1] = -3;
    points[2][0] = 3;
    points[2][1] = 4;

    int p1,p2,p3;

    getCclockwise(points, p1, p2, p3, 0, 1, 2);
    getCircumcentre(points, p1, p2, p3, points[3]);

    std::cout << p1 << ' ' << p2 << ' ' << p3 << std::endl;
    std::cout << points[p1][0] << ' ' << points[p1][1] << std::endl;
    std::cout << points[p2][0] << ' ' << points[p2][1] << std::endl;
    std::cout << points[p3][0] << ' ' << points[p3][1] << std::endl;

    std::cout << result[0] << ' ' << result[1] << std::endl;

    std::cout << isEmpty(points, 4, p1, p2, p3) << std::endl;

    float a,b,c, currRadius;
    a = sqrt((points[p1][0] - points[p2][0]) * (points[p1][0] - points[p2][0]) + (points[p1][1] - points[p2][1]) * (points[p1][1] - points[p2][1]));
    b = sqrt((points[p1][0] - points[p3][0]) * (points[p1][0] - points[p3][0]) + (points[p1][1] - points[p3][1]) * (points[p1][1] - points[p3][1]));
    c = sqrt((points[p2][0] - points[p3][0]) * (points[p2][0] - points[p3][0]) + (points[p2][1] - points[p3][1]) * (points[p2][1] - points[p3][1]));  
    currRadius = (a*b*c)/(sqrt((a+b+c) * (-a+b+c) * (a-b+c) * (a+b-c)));

    std::cout << currRadius << std::endl;
}

void testLocalCoords(){
    float* retCoords = new float[2];
    float* params = new float[10]{};

    Vertex point, normal, origin;

    origin = Vertex(1,1,1);
    normal = Vertex(1/sqrt(2),0,1/sqrt(2));

    point = Vertex(10,0,10);

    getLocalCoords(retCoords, point, origin, normal);

    std::cout << retCoords[0] << ' ' << retCoords[1] << std::endl;

    std::cout << retCoords[0] << ' ' << retCoords[1] << std::endl;

    getWorldCoords(retCoords, &point, origin, normal, params);
    std::cout << point << std::endl;
}


void postProcessNormals(Vertex* vert, Grid *grid){
    if(vert->isPP){
        return;
    }
    std::vector<Vertex*> nbrs;
    int counter = 0;
    
    getClosestCells(grid, vert, nbrs, false);
    
    for(size_t j = 0; j < nbrs.size(); ++j){
        if(*vert->normal * *nbrs[j]->normal < -0.8){
            ++counter;
        }
    }

    vert->isPP = true;
    if (counter > nbrs.size()/10.0 * 8.0){
        *vert->normal = - *vert->normal;
    }
    for(size_t j = 0; j < nbrs.size(); ++j){
        postProcessNormals(nbrs[j], grid);
        
    }
    nbrs.clear();
}

void defaultDownSample(Vertex** verts, int N){
    std::default_random_engine gen(std::random_device{}());
    std::uniform_int_distribution<>distr(0,N-1);

    std::vector<int> excluded;

    for(int i = 0; i < N; ++i){
        if(i%2 && i%3){
            std::cout << verts[i]->x << ' ' << verts[i]->y << ' ' << verts[i]->z << std::endl;
        }
    }
}