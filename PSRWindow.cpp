#include "PSRWindow.h"

PSRWindow::PSRWindow(Model *model, QWidget *parent): QWidget(parent){
	this->model = model;

	psrWidget = new PSRWidget(model, this);

	windowLayout = new QHBoxLayout(this);

	optionGroup = new QGroupBox("Options", this);
	projectionOptionGroup = new QGroupBox("Projection options", this);
	widgetGroup = new QGroupBox("Render", this);
	renderOptionGroup = new QGroupBox("Render options", this);

	optionLayout = new QVBoxLayout(optionGroup);
	widgetLayout = new QVBoxLayout(widgetGroup);
	renderOptionLayout = new QVBoxLayout(renderOptionGroup);
	projectionOptionLayout = new QVBoxLayout(projectionOptionGroup);

	scaleSlider = new QSlider(Qt::Horizontal);
	levelSlider = new QSlider(Qt::Horizontal);
	downSlider = new QSlider(Qt::Horizontal);
	upSlider = new QSlider(Qt::Horizontal);
	radiusSlider = new QSlider(Qt::Horizontal);
	hSlider = new QSlider(Qt::Horizontal);

	scaleLabel = new QLabel("Scale");
	levelLabel = new QLabel("Sphere level");
	downLabel = new QLabel("Down-sampling");
	upLabel = new QLabel("Up-sampling");
	radiusLabel = new QLabel("Patch radius");
	hLabel = new QLabel("Feature size (h)");

	projectSurfButton = new QPushButton("Project surface");
	exportPPMButton = new QPushButton("Export PPM");

	renderPointButton = new QCheckBox("Points");
	renderProjPointButton = new QCheckBox("Projected Points");
	renderOctreeButton = new QCheckBox("Octree Grid");
	renderRepresentationPointButton = new QCheckBox("Representation Points");
	renderNormalButton = new QCheckBox("Normals");
	renderSurfaceButton = new QCheckBox("Surface");
	renderCompNormalButton = new QCheckBox("Computed normals");
	renderSpheresButton = new QCheckBox("Hierarchical bounding spheres");


	projectionOptionLayout->addWidget(hLabel);

	hSlider->setMinimum(1);
	hSlider->setMaximum(100);
	hSlider->setValue(15);
	
	projectionOptionLayout->addWidget(hSlider);
	projectionOptionLayout->addWidget(projectSurfButton);

	renderOptionLayout->addWidget(scaleLabel);

	scaleSlider->setMinimum(1);
	scaleSlider->setMaximum(100);
	scaleSlider->setValue(50);
	renderOptionLayout->addWidget(scaleSlider);

	renderOptionLayout->addWidget(levelLabel);
	
	levelSlider->setMinimum(0);
	levelSlider->setMaximum(20);
	levelSlider->setValue(0);
	renderOptionLayout->addWidget(levelSlider);

	renderOptionLayout->addWidget(downLabel);
	
	downSlider->setMinimum(0);
	downSlider->setMaximum(30);
	downSlider->setValue(0);
	renderOptionLayout->addWidget(downSlider);

	renderOptionLayout->addWidget(upLabel);
	
	upSlider->setMinimum(0);
	upSlider->setMaximum(500);
	upSlider->setValue(0);
	renderOptionLayout->addWidget(upSlider);

	renderOptionLayout->addWidget(radiusLabel);
	
	radiusSlider->setMinimum(1);
	radiusSlider->setMaximum(200);
	radiusSlider->setValue(10);
	renderOptionLayout->addWidget(radiusSlider);

	renderOptionLayout->addWidget(renderPointButton);
	renderOptionLayout->addWidget(renderProjPointButton);
	renderOptionLayout->addWidget(renderRepresentationPointButton);
	renderOptionLayout->addWidget(renderOctreeButton);
	renderOptionLayout->addWidget(renderCompNormalButton);
	renderOptionLayout->addWidget(renderSpheresButton);
	renderOptionLayout->addWidget(renderSurfaceButton);

	
	optionLayout->addWidget(projectionOptionGroup);
	optionLayout->addWidget(renderOptionGroup);
	optionLayout->addWidget(exportPPMButton);

	optionLayout->addStretch();
	widgetLayout->addWidget(psrWidget);

	optionGroup->setMaximumWidth(300);
	
	windowLayout->addWidget(widgetGroup);
	windowLayout->addWidget(optionGroup);

}

PSRWindow::~PSRWindow(){ 
} 


void PSRWindow::ResetInterface(){
	psrWidget->update();
	update();
}
