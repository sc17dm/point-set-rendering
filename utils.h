#pragma once

#include "vertex.h"
#include "octree.h"
#include "grid.h"
#include <math.h>
#include <iostream>
#include <assert.h>
#include <queue> 
#include <set>
#include <map>
#include <iterator>
#include <random>
#include <limits.h>

#define TOL 2.0e-4f
#define ITMAX 200
#define EPS 1.0e-10
#define ZEPS 1.0e-10
#define GOLD 1.618034
#define GLIMIT 100.0
#define TINY 1.0e-20
#define PI 3.1415
#define MOV3(a, b, c,  d, e, f) (a)=(d);(b)=(e);(c)=(f);
#define SHIFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
#define SGN(a) ((a) >= 0.0 ? 1 : -1)
#define FMAX(a,b) ((a) > (b) ? (a) : (b))
#define MAXVOROSIZE 0.00003

extern const int nbrCells[8][8][3];

float dbrent(float bracketA, float bracketB, float bracketC, float (*function)(float), float (*derivative)(float), float tol, float* xmin);

float dbrent(float bracketA, float bracketB, float bracketC, float tol, float* xmin, int N, Vertex** verts, float(*radWeight)(float, float), Vertex normal, Vertex point, float h);

float dbrent(float bracketA, float bracketB, float bracketC, float tol, float* xmin, int N, std::vector<Vertex*> verts, float(*radWeight)(float, float), Vertex normal, Vertex point, float h);

void conjGradient(Vertex *p, float ftol, float *fret, std::vector<Vertex*> verts, int N, Vertex normal, float(*radWeight)(float,float), float h_rad);

void dlinmin(Vertex *p, Vertex *xi, float *fret, float (*func)(Vertex), void (*dfunc)(Vertex, Vertex*));

void dlinmin(Vertex* p, Vertex* xi, float *fret);

float sigma(float d, float h);
// function to minimize t
float mlsFunction(int N, Vertex** verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h);
float mlsFunction(int N, std::vector<Vertex*> verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h);
// derivative to minimize t
float mlsDerivative(int N, Vertex** verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h);
float mlsDerivative(int N, std::vector<Vertex*> verts, float(*radWeight)(float, float), float t, Vertex normal, Vertex point, float h);
// function to minimize q
float qFunction(int N, Vertex** verts, Vertex** projections, float(*radWeight)(float, float), float (*polynomial)(float*, float, float), float *params, Vertex normal, Vertex q, float h);
// derivative to minimize q
float qDerivative(int N, Vertex** verts, Vertex** projections, float(*radWeight)(float, float), float (*polynomial)(float*, float, float), float *params, Vertex normal, Vertex q, float h);

float polynomial3(float *params, float x, float y);

Vertex gradient(float params, Vertex point);

float df1dim(float x);

float f1dim(float x);

void mnbrak(float* ax, float *bx, float *cx, float *fa, float *fb, float *fc, float (*func)(float));

float funcCoeff(int coeff, float x, float y);

void getLocalCoords(float* returnCoords, Vertex point, Vertex origin, Vertex normal);

void GaussJ(float** matrix, int n, float* b);

void LUDecomposition(float** matrix, int n, int* index);

void LUSolving(float** matrix, int n, int* index, float* b);

void projectSurface(float h, int N, Vertex** verts, float(*radWeight)(float, float), float epsilon, float (*polynomial)(float*, float, float), Grid* grid);

float mini(float a, float b);

void getGCoefficients(float* params, Vertex q, Vertex normal, float h, int N, Vertex** verts, float(*radWeight)(float, float), Vertex** projections);

void getGCoefficients(float* params, Vertex q, Vertex normal, float h, int N, std::vector<Vertex*> verts, float(*radWeight)(float, float));

double solveCubic(double a, double b, double c);

void computeNormals(Vertex** verts, int N, Vertex* r, Vertex *normal, float(*radWeight)(float,float), float h);

void computeNormals(std::vector<Vertex*> verts, int N, Vertex* r, Vertex *normal, float(*radWeight)(float,float), float h);

float qFuncCG(std::vector<Vertex*> verts, int N, Vertex q, Vertex normal, float(*radWeight)(float,float), float h);

void qDerivCG(std::vector<Vertex*> verts, int N, Vertex q, Vertex *res, Vertex normal, float(*radWeight)(float,float), float h);

void getClosestCells(Grid* grid, Vertex* vert, std::vector<Vertex*> &vertices, bool shouldIncludeItself);

void getVertices(Octree* oct, std::vector<Vertex*>& vertices, Vertex* vert);

void getWorldCoords(float* returnCoords, Vertex* point, Vertex origin, Vertex normal, float* params);

void downSampler(Vertex** verts, int N, int percentage, Grid* grid);

void testEigen(double ** cM);

void testOctree(Vertex** verts, int N, Grid* grid);

void getCclockwise(float** points, int& p1, int& p2, int& p3, int i, int j, int k);

void getCircumcentre(float** points, int p1, int p2, int p3, float* result);

bool isEmpty(float** points, int n, int p1, int p2, int p3);

void getVoronoiVertex(float** points, int n, float* result);

void upSampler(Vertex** verts, std::vector<Vertex*>& vertices, int N, int percentage, Grid* grid);

void testVoronoi();

void testLocalCoords();

void postProcessNormals(Vertex* vert, Grid *grid);

void defaultDownSample(Vertex** verts, int N);