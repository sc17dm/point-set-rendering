#pragma once

#include <cmath>
#include <iostream>
#include <vector>

class Vertex{
    public:
        Vertex();
        Vertex(const Vertex &old);
        Vertex(float x, float y, float z);

        float x;
        float y;
        float z;
        float t;
        Vertex* projection;
        Vertex* normal;
        float* params;
        int shouldDraw;
        float priority;
        int upLevel;
        bool isPP;
        std::vector<Vertex*> upSamplers;

        bool operator == (const Vertex &rhs) const;
        Vertex operator + (const Vertex &rhs) const;
        Vertex& operator += (const Vertex &rhs);
        Vertex operator - () const;
        Vertex operator - (const Vertex &rhs) const;
        Vertex& operator -= (const Vertex &rhs);
        float operator * (const Vertex &rhs) const;
        Vertex operator * (float const &rhs) const;
        Vertex& operator *= (float const &rhs);
        float& operator [](int index);
        float dist(const Vertex &rhs) const;
        float dist() const;
        Vertex cross(const Vertex &rhs) const;
        Vertex cross() const;
        Vertex normalize() const;
        friend std::ostream& operator<<(std::ostream& ostr, const Vertex& rhs);
};