#pragma once
#include "vertex.h"
#include "octree.h"
#include <vector>
#include <iostream>
#include <cmath>

class Grid{
    public:
        Grid();
        Grid(float x, float y, float z, float NeglDist, float XMin, float YMin, float ZMin);
        int xSize,ySize,zSize;
        float xMin,yMin,zMin;
        Octree** cells;
        float neglDist;
        Octree* retVal;

        Octree*& operator() (int x, int y, int z);
        friend std::ostream& operator<<(std::ostream& ostr, Grid& rhs);
};



